// In App.js in a new project

import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Platform,
  Alert,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import {
  createStackNavigator,
  HeaderBackButton,
} from '@react-navigation/stack';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import { Grid, Col, Row } from 'react-native-easy-grid';

import { Root, Thumbnail, Label, Accordion, Icon } from 'native-base';

import LoginScreen from './screens/LoginScreen';
import HomeScreen from './screens/HomeScreen';
import { UserContext } from './context/UserContext';
import DetailedScheduleScreen from './screens/DetailedScheduleScreen';
import MaintenanceScreen from './screens/MaintenanceScreen';
import NewAssemblyScreen from './screens/NewAssemblyScreen';
import FinishScheduleModal from './screens/FinishScheduleModal';
import ManageBranchCalendarScreens from './screens/ManageBranchCalendarScreens';
import PostponeScheduleModal from './screens/PostponeScheduleModal';
import CancelScheduleModal from './screens/CancelScheduleModal';
import CalendarTransferScreens from './screens/CalendarTransferScreens';
import ScheduleSearchScreen from './screens/ScheduleSearchScreen';
import ClientLookupScreen from './screens/ClientLookupScreen';
import CustomerInformationScreen from './screens/CustomerInformationScreen';
import ChangeDeviceScreen from './screens/ChangeDeviceScreen';
import DeviceRetrievalScreen from './screens/DeviceRetrievalScreen';
import SearchDeviceScreen from './screens/SearchDeviceScreen';
import EnterTheBranchWarehouseScreen from './screens/EnterTheBranchWarehouseScreen';
import WarehouseManagementScreen from './screens/WarehouseManagementScreen';
import SearchScreenCancelWarehouseExport from './screens/SearchScreenCancelWarehouseExport';
import ListResultsCancelWarehouseExport from './screens/ListResultsCancelWarehouseExport';
import WarehouseExportCancelScreen from './screens/WarehouseExportCancelScreen';
import BarcodeScreen from './screens/BarcodeScreen';
import BarcodeScannerScreen from './screens/BarcodeScannerScreen';
import CollectMoneyScreen from './screens/CollectMoneyScreen';
import SearchBillScreen from './screens/SearchBillScreen';
import IndoorCalendar from './screens/IndoorCalendar';
import DetailedIndoorSchedule from './screens/DetailedIndoorSchedule';
import AddWifiScreen from './screens/AddWifiScreen';
import FinishedIndoorCalendar from './screens/FinishedIndoorCalendar';
import DeviceRetrievalIndoorScreen from './screens/DeviceRetrievalIndoorScreen';
import ManageIndoorCalendar from './screens/ManageIndoorCalendar';
import CreateSchedule from './screens/CreateSchedule';
import SuppliesList from './screens/SuppliesList';
import ChangePassword from './screens/ChangePassword';

import { useFonts } from 'expo-font';

export default () => {
  const Stack = createStackNavigator();
  const Drawer = createDrawerNavigator();
  const [user, setUser] = React.useState(null);
  const store = {
    user: user,
    setUser: setUser,
  };
  const [loaded] = useFonts({
    Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
  });
  const [dataMenus, setData] = React.useState([
    {
      title: 'Lịch',
      content:
        'Lịch của tôi:Home, Tra cứu lịch:ScheduleSearchScreen',
    },
    {
      title: 'Lịch chủ động',
      content:
        'Lịch chủ động của tôi:IndoorCalendar, Lịch đã hoàn tất:FinishedIndoorCalendar',
    },
    // {
    //   title: 'Lịch Indoor',
    //   content:
    //     'Lịch Indoor của tôi:IndoorCalendar, Lịch đã hoàn tất:FinishedIndoorCalendar, Quản lý lịch Indoor:ManageIndoorCalendar',
    // },
    { title: 'Khách hàng', content: 'Tra cứu khách hàng:ClientLookupScreen' },
    {
      title: 'Thiết bị',
      content:
        'Tra cứu thiết bị:SearchDeviceScreen',
    },
    // {
    //   title: 'Thu tiền',
    //   content:
    //     'Thu tiền bắn mã:BarcodeScreen, Báo cáo thu tiền:SearchBillScreen', Quản lý lịch chi nhánh:ManageBranchCalendarScreens,
    // },
    {
      title: 'Cấu hình',
      content:
        'Thay đổi mật khẩu:ChangePassword, Đăng xuất:LogOut',
    },
  ]);
  React.useEffect(() => {
    let data = [
      {
        title: 'Lịch',
        content:
          'Lịch của tôi:Home, Tra cứu lịch:ScheduleSearchScreen',
      },
      {
        title: 'Lịch chủ động',
        content:
          'Lịch chủ động của tôi:IndoorCalendar, Lịch đã hoàn tất:FinishedIndoorCalendar',
      },
      // {
      //   title: 'Lịch Indoor',
      //   content:
      //     'Lịch Indoor của tôi:IndoorCalendar, Lịch đã hoàn tất:FinishedIndoorCalendar, Quản lý lịch Indoor:ManageIndoorCalendar',
      // },
      { title: 'Khách hàng', content: 'Tra cứu khách hàng:ClientLookupScreen' },
      {
        title: 'Thiết bị',
        content:
          'Tra cứu thiết bị:SearchDeviceScreen',
      },
      // {
      //   title: 'Thu tiền',
      //   content:
      //     'Thu tiền bắn mã:BarcodeScreen, Báo cáo thu tiền:SearchBillScreen', Quản lý lịch chi nhánh:ManageBranchCalendarScreens,
      // },
      {
        title: 'Cấu hình',
        content:
          'Thay đổi mật khẩu:ChangePassword, Đăng xuất:LogOut',
      },
    ];
    if (user && user.QuyenThucThi.includes('HDS_CN_QUANLY_KHACHHANG_YEUCAU')) {
      data.splice(0, 1, {
        title: 'Lịch',
        content:
          'Lịch của tôi:Home, Quản lý lịch chi nhánh:ManageBranchCalendarScreens, Tra cứu lịch:ScheduleSearchScreen',
      })
    }
    if (user && user.QuyenThucThi.includes('APP_THUTIEN')) {
      data.splice(4, 0, {
        title: 'Thu tiền',
        content:
          'Thu tiền bắn mã:BarcodeScreen, Lịch sử thu tiền:SearchBillScreen',
      });
    }
    if (user && user.QuyenThucThi.includes('KTS_QUANLY_THIETBI')) {
      data.splice(3, 1, {
        title: 'Thiết bị',
        content:
          'Tra cứu thiết bị:SearchDeviceScreen, Nhập kho chi nhánh:EnterTheBranchWarehouseScreen, Quản lý xuất kho:WarehouseManagementScreen, Hủy xuất kho:SearchScreenCancelWarehouseExport',
      })
    }
    setData(data);
  }, [user])
  const CustomDrawerContent = (props) => {
    global.navigation = props.navigation;
    return (
      <DrawerContentScrollView {...props}>
        <View style={styles.avtBlock}>
          <Thumbnail large source={require('./assets/img/Logo_SCTV.png')} />
          <Label style={styles.info}>
            {user.ND_TEN_NGUOI_DUNG} {user.DIEN_THOAI}
          </Label>
        </View>
        <Accordion
          dataArray={dataMenus}
          style={{ borderWidth: 0 }}
          animation={true}
          expanded={true}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
        />
        {/* <TouchableOpacity
          style={{ marginLeft: 16, marginTop: 16 }}
          onPress={() => {
            setUser(null);
          }}>
          <Text style={{ fontWeight: 'bold' }}>Đăng xuất</Text>
        </TouchableOpacity> */}
        <View style={{ marginLeft: 16, marginTop: 16 }}><Text>Version 1.1.5</Text></View>
      </DrawerContentScrollView>
    );
  };
  const _renderHeader = (item, expanded) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          padding: 10,
          margin: 5,
          borderBottomWidth: 1,
          borderColor: 'rgba(0, 0, 0, 0.1)',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}>
        <Text style={{ fontWeight: 'bold' }}> {item.title}</Text>
        {expanded ? (
          <Icon
            style={{ fontSize: 24 }}
            type="MaterialIcons"
            name="keyboard-arrow-up"
          />
        ) : (
          <Icon
            style={{ fontSize: 24 }}
            type="MaterialIcons"
            name="keyboard-arrow-down"
          />
        )}
      </View>
    );
  };
  const onNavigate = (item) => {
    if (item === 'Đăng xuất:LogOut') {
      setUser(null);
    }
    else global.navigation.navigate(item.split(':')[1]);
  }
  const _renderContent = (item) => {
    let items = item.content.split(', ');
    // console.log(items);
    return items.map((item, key) => (
      <TouchableOpacity
        key={key}
        style={{ marginLeft: 20 }}
        onPress={() => onNavigate(item)}>
        <Text style={{ margin: 10, paddingLeft: 10 }}>
          {item.split(':')[0]}
        </Text>
      </TouchableOpacity>
    ));
  };
  const StackMain = () => {
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{
            title: 'Lịch của tôi',
            headerTitleAlign: 'center',
            headerLeft: (props) => (
              <Icon
                name={Platform.OS === 'ios' ? 'ios-menu' : 'md-menu'}
                onPress={() => global.navigation.openDrawer()}
                style={{ paddingLeft: 10 }}
              />
            ),
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="DetailedSchedule"
          component={DetailedScheduleScreen}
          options={{
            title: 'Thông tin lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="MaintenanceScreen"
          component={MaintenanceScreen}
          options={{
            title: 'Thiết bị bảo trì',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="FinishScheduleModal"
          component={FinishScheduleModal}
          options={{
            title: 'Hoàn tất lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="NewAssemblyScreen"
          component={NewAssemblyScreen}
          options={{
            title: 'Thiết bị lắp mới',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="PostponeScheduleModal"
          component={PostponeScheduleModal}
          options={{
            title: 'Hẹn lại',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ManageBranchCalendarScreens"
          component={ManageBranchCalendarScreens}
          options={{
            title: 'Quản lý lịch chi nhánh',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="CancelScheduleModal"
          component={CancelScheduleModal}
          options={{
            title: 'Hủy lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="CalendarTransferScreens"
          component={CalendarTransferScreens}
          options={{
            title: 'Sắp lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ScheduleSearchScreen"
          component={ScheduleSearchScreen}
          options={{
            title: 'Tra cứu lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ClientLookupScreen"
          component={ClientLookupScreen}
          options={{
            title: 'Tra cứu khách hàng',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="CustomerInformationScreen"
          component={CustomerInformationScreen}
          options={{
            title: 'Thông tin khách hàng',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ChangeDeviceScreen"
          component={ChangeDeviceScreen}
          options={{
            title: 'Đổi thiết bị',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="DeviceRetrievalScreen"
          component={DeviceRetrievalScreen}
          options={{
            title: 'Thu hồi thiết bị',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="SearchDeviceScreen"
          component={SearchDeviceScreen}
          options={{
            title: 'Tìm kiếm thiết bị',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="EnterTheBranchWarehouseScreen"
          component={EnterTheBranchWarehouseScreen}
          options={{
            title: 'Nhập kho chi nhánh',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="WarehouseManagementScreen"
          component={WarehouseManagementScreen}
          options={{
            title: 'Quản lý xuất kho',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="SearchScreenCancelWarehouseExport"
          component={SearchScreenCancelWarehouseExport}
          options={{
            title: 'Tìm kiếm hủy xuất kho',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ListResultsCancelWarehouseExport"
          component={ListResultsCancelWarehouseExport}
          options={{
            title: 'Danh sách hủy xuất kho',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="WarehouseExportCancelScreen"
          component={WarehouseExportCancelScreen}
          options={{
            title: 'Hủy xuất kho',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="BarcodeScreen"
          component={BarcodeScreen}
          options={{
            title: 'Bắn mã',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="BarcodeScannerScreen"
          component={BarcodeScannerScreen}
          options={{
            headerShown: false,
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="CollectMoneyScreen"
          component={CollectMoneyScreen}
          options={{
            title: 'Thu tiền',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="SearchBillScreen"
          component={SearchBillScreen}
          options={{
            title: 'Lịch sử phiếu thu',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="IndoorCalendar"
          component={IndoorCalendar}
          options={{
            title: 'Lịch chủ động của tôi',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="DetailedIndoorSchedule"
          component={DetailedIndoorSchedule}
          options={{
            title: 'Thông tin lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="AddWifiScreen"
          component={AddWifiScreen}
          options={{
            title: 'Thêm thiết bị wifi',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="FinishedIndoorCalendar"
          component={FinishedIndoorCalendar}
          options={{
            title: 'Lịch đã hoàn tất',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="DeviceRetrievalIndoorScreen"
          component={DeviceRetrievalIndoorScreen}
          options={{
            title: 'Thu hồi thiết bị chủ động',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ManageIndoorCalendar"
          component={ManageIndoorCalendar}
          options={{
            title: 'Quản lý lịch chủ động',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="CreateSchedule"
          component={CreateSchedule}
          options={{
            title: 'Tạo lịch',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="SuppliesList"
          component={SuppliesList}
          options={{
            title: 'Danh sách vật tư',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
        <Stack.Screen
          name="ChangePassword"
          component={ChangePassword}
          options={{
            title: 'Thay đổi mật khẩu',
            headerTitleAlign: 'center',
            safeAreaInsets: { top: 0, bottom: 0 }
          }}
        />
      </Stack.Navigator>
    );
  };
  if (!loaded)
    return null;

  return (
    <Root>
      <UserContext.Provider value={store}>
        {user == null ? (
          <LoginScreen />
        ) : (
          <NavigationContainer>
            <Drawer.Navigator
              drawerContent={(props) => <CustomDrawerContent {...props} />}>
              <Drawer.Screen name="Menu" component={StackMain} />
            </Drawer.Navigator>
          </NavigationContainer>
        )}
      </UserContext.Provider>
    </Root>
  );
};
const styles = StyleSheet.create({
  avtBlock: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 30,
  },
  info: {
    marginTop: 10,
    fontWeight: 'bold',
  },
});
