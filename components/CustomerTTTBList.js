//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import ListThueBaoModel from '../models/ListThueBaoModel';

type Props = {
  item: ListThueBaoModel,
  index: number,
  onPressItem: (customer: ListThueBaoModel) => void,
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    // <TouchableOpacity onPress={()=>props.onPressItem(props.item)}>
    <Card
      style={{
        padding: 8,
        backgroundColor: backgroundColor,
      }}>
      <Text>
        - {props.item.TB_MATHUEBAO}, {props.item.TEN_DICHVU}(
        {props.item.TEN_GOICUOC_CHITIET}), [{props.item.TEN_TRANGTHAI}]
      </Text>
    </Card>
    // </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<ListThueBaoModel>,
  // onPressItem:(customer:ListThueBaoModel)=>void
};
export const CustomerTTTBList = (props: CustomerListProps) => {
  return (
    <FlatList
      style={{padding: 8}}
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        return <CustomerListItem item={item} index={index} />;
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};
