import React, { useState } from 'react';
import {
  Thumbnail,
  Container,
  Content,
  Toast,
  Input,
  Text,
  Label,
  View,
  Item,
  Form,
  Picker,
  Icon,
  List,
  ListItem,
  Left,
  Right,
  Body,
  Button,
  ActionSheet,
} from 'native-base';
import {
  StyleSheet,
  Dimensions,
  Alert,
  Modal,
  TouchableOpacity,
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { ThietBiRow } from '../models/GroupThietBi';
import closeIcon from '../assets/img/close_icon.png';
import MultiSelect from 'react-native-multiple-select';

var tempList = [];
var listStatus = ['C', 'M'];
var listPayment = ['MienPhi', 'CoPhi'];
var list = {
  C: 'Cũ',
  M: 'Mới',
  CoPhi: 'Có phí',
  MienPhi: 'Miễn phí',
};
var DESTRUCTIVE_INDEX = 3;
var CANCEL_INDEX = 4;
const ModalAddThietBi = React.forwardRef((props, ref) => {
  // const ModalAddThietBi = () => {
  let multiSelect = null;
  const [modalVisible, setModalVisible] = useState(false);
  const [disable, setDisable] = useState(true);
  const [state, setState] = useState({
    selectedEquip: '',
    id: '',
    // selectedStatus: 'Chọn tình trạng',
    unit: '',
    selectedPayment: 'Loại thanh toán',
    selectedItems: ['Chọn thiết bị'],
  });
  const [listMaterial, setList] = React.useState([]);
  const resetState = () => {
    setState({
      selectedEquip: '',
      id: '',
      // selectedStatus: 'Chọn tình trạng',
      unit: '',
      selectedPayment: 'Loại thanh toán',
      index: '',
      selectedItems: ['Chọn thiết bị'],
    });
    setList([]);
    tempList = [];
  };
  const onThietBiChange = (selectedItems) => {
    setState({
      ...state,
      selectedItems: selectedItems,
      unit: global.listMaterial.find((element) => {
        if (element.Id === selectedItems[0]) return element;
      }).Value,
      selectedEquip: global.listMaterial.find((element) => {
        if (element.Id === selectedItems[0]) return element;
      }).Text,
    });
  };
  const onSave = () => {
    if (state.amount <= 0) {
      Toast.show({
        text: 'Vui lòng nhân viên kiểm tra số lượng thiết bị.',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      let item: ThietBiRow = {
        DMTB_ID: state.selectedItems[0],
        LOAI_THANH_TOAN: state.selectedPayment,
        SO_LUONG: state.amount,
        TenThietBi: state.selectedEquip,
        // TINHTRANG_VATTU: state.selectedStatus,
        LOAI_THANH_TOAN: state.selectedPayment,
        TINHTRANG_VATTU: 'M',
        Unit: state.unit,
      };
      props.addItem(state.index, item);
      resetState();
      setModalVisible(false);
    }
  };
  React.useImperativeHandle(ref, () => ({
    showModal(index) {
      setModalVisible(true);
      setState({ ...state, index: index });
    },
  }));
  const onChangeDisable = () => {
    if (
      state.selectedItems[0] != 'Chọn thiết bị' &&
      state.amount > 0
      // state.selectedStatus != 'Chọn tình trạng' &&
      && state.selectedPayment != 'Loại thanh toán'
    ) {
      setDisable(false);
    } else setDisable(true);
  };
  React.useEffect(() => {
    onChangeDisable();
  });
  return (
    // <View style={styles.centeredView}>
    <Modal
      animationType="slide"
      transparent={true}
      visible={modalVisible}
      onRequestClose={() => {
        setModalVisible(!modalVisible);
      }}>
      <Container>
        <View style={styles.centeredView}>
          <Content>
            <View style={styles.mainModalView}>
              <TouchableOpacity
                style={styles.closeBtn}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}>
                <Thumbnail
                  square
                  source={closeIcon}
                  style={{ width: 30, height: 30 }}
                />
              </TouchableOpacity>
              <Grid style={{ borderWidth: 0.5, marginBottom: 10 }}>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}><Text style={styles.titleTxt}>Tên thiết bị</Text></Col>
                  <Col style={styles.rightBlock}>
                    <MultiSelect
                      hideTags
                      single={true}
                      fontSize={15}
                      hideDropdown={false}
                      items={
                        global.listMaterial.length > 0
                          ? global.listMaterial
                          : []
                      }
                      uniqueKey="Id"
                      ref={(component) => {
                        multiSelect = component;
                      }}
                      onSelectedItemsChange={onThietBiChange}
                      selectedItems={state.selectedItems}
                      selectText="Chọn thiết bị"
                      searchInputPlaceholderText="Tìm kiếm thiết bị..."
                      styleListContainer={{ backgroundColor: '#ccc' }}
                      styleMainWrapper={{ margin: 5 }}
                      // altFontFamily="ProximaNova-Light"
                      tagRemoveIconColor="#CCC"
                      tagBorderColor="#CCC"
                      tagTextColor="#CCC"
                      textColor="#000"
                      selectedItemTextColor="#fff"
                      selectedItemIconColor="#fff"
                      itemTextColor="#000"
                      displayKey="Text"
                      searchInputStyle={{ color: '#ccc' }}></MultiSelect>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}><Text style={styles.titleTxt}>Số lượng</Text></Col>
                  <Col style={styles.rightBlock}>
                    <Item>
                      <Input
                        placeholder="Nhập số lượng"
                        keyboardType="numeric"
                        placeholderTextColor="grey"
                        onChangeText={(amount) =>
                          setState({ ...state, amount: amount })
                        }
                      />
                      <View style={{ flex: 0.3, justifyContent: 'center' }}>
                        <Text>{state.unit}</Text>
                      </View>
                    </Item>

                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}><Text style={styles.titleTxt}>Loại thanh toán</Text></Col>
                  <Col style={styles.rightBlock}>
                    <TouchableOpacity
                      onPress={() => {
                        ActionSheet.show(
                          {
                            options: ['Miễn phí', 'Có phí'],
                            cancelButtonIndex: CANCEL_INDEX,
                            destructiveButtonIndex: DESTRUCTIVE_INDEX,
                            title: 'Hình thức thanh toán',
                          },
                          (buttonIndex) => {
                            if (buttonIndex < 2) {
                              setState({
                                ...state,
                                selectedPayment: listPayment[buttonIndex],
                              });
                            } else {
                              setState({
                                ...state,
                                selectedPayment: listPayment[0],
                              });
                            }
                          }
                        );
                      }}>
                      {state.selectedPayment === 'Loại thanh toán' ? (
                        <Text>{state.selectedPayment}</Text>
                      ) : (
                        <Text>{list[state.selectedPayment]}</Text>
                      )}
                    </TouchableOpacity>
                  </Col>
                </Row>
                {/* <Row style={styles.row}>
                  <Col style={styles.leftBlock}><Text style={styles.titleTxt}>Tình trạng vật tư</Text></Col>
                  <Col style={styles.rightBlock}>
                    <TouchableOpacity
                      onPress={() => {
                        ActionSheet.show(
                          {
                            options: ['Cũ', 'Mới'],
                            cancelButtonIndex: CANCEL_INDEX,
                            destructiveButtonIndex: DESTRUCTIVE_INDEX,
                            title: 'Chọn tình trạng',
                          },
                          (buttonIndex) => {
                            if (buttonIndex < 2) {
                              setState({
                                ...state,
                                selectedStatus: listStatus[buttonIndex],
                              });
                            } else {
                              setState({
                                ...state,
                                selectedStatus: listStatus[0],
                              });
                            }
                          }
                        );
                      }}>
                      {state.selectedStatus === 'Chọn tình trạng' ? (
                        <Text>{state.selectedStatus}</Text>
                      ) : (
                        <Text>{list[state.selectedStatus]}</Text>
                      )}
                    </TouchableOpacity>
                  </Col>
                </Row> */}

                {/* <Col>
              <Row style={styles.row}>
                <Text style={styles.titleTxt}>Tên thiết bị</Text>
              </Row>
              <Row style={styles.row}>
                <Text style={styles.titleTxt}>Số lượng</Text>
              </Row>
              <Row style={styles.row}>
                <Text style={styles.titleTxt}>Tình trạng vật tư</Text>
              </Row>
              <Row style={styles.row}>
                <Text style={styles.titleTxt}>Loại thanh toán</Text>
              </Row>
            </Col>
            <Col>
              <Row style={styles.row}>
                <TouchableOpacity
                  onPress={() => {
                    if(listMaterial != []){
                      listMaterial = [];
                    }
                    global.listMaterial.forEach((item) => {
                      listMaterial.push(item.Text);
                    });
                    ActionSheet.show(
                      {
                        options: listMaterial,
                        cancelButtonIndex: CANCEL_INDEX,
                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                        title: 'Chọn thiết bị',
                      },
                      (buttonIndex) => {
                        // setState({...state, selectedEquip: listMaterial[buttonIndex]});
                        onThietBiChange(listMaterial[buttonIndex]);
                      }
                    );
                  }}>
                  <Text style={{ fontStyle: 'italic' }}>
                    {state.selectedEquip}
                  </Text>
                </TouchableOpacity>
                <MultiSelect
                  hideTags
                  single={true}
                  fontSize={15}
                  hideDropdown = {false}
                  items={
                    global.listMaterial.length > 0
                      ? global.listMaterial
                      : []
                  }
                  uniqueKey="Id"
                  ref={(component) => {
                    multiSelect = component;
                  }}
                  onSelectedItemsChange={onSelectedItemsChange}
                  selectedItems={state.selectedItems}
                  selectText="Chọn thiết bị"
                  searchInputPlaceholderText="Tìm kiếm thiết bị..."
                  altFontFamily="ProximaNova-Light"
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  textColor="#000"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="Text"
                  searchInputStyle={{ color: '#000' }}></MultiSelect>
              </Row>
              <Row style={[styles.row, { flexDirection: 'row' }]}>
                <Input
                  placeholder="Nhập số lượng"
                  keyboardType="numeric"
                  placeholderTextColor="grey"
                  onChangeText={(amount) =>
                    setState({ ...state, amount: amount })
                  }
                />
                <View style={{ flex: 0.3, justifyContent: 'center' }}>
                  <Text>{state.unit}</Text>
                </View>
              </Row>
              <Row style={styles.row}>
                <TouchableOpacity
                  onPress={() => {
                    ActionSheet.show(
                      {
                        options: ['Cũ', 'Mới'],
                        cancelButtonIndex: CANCEL_INDEX,
                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                        title: 'Chọn tình trạng',
                      },
                      (buttonIndex) => {
                        if (buttonIndex < 2) {
                          setState({
                            ...state,
                            selectedStatus: listStatus[buttonIndex],
                          });
                        } else {
                          setState({
                            ...state,
                            selectedStatus: listStatus[0],
                          });
                        }
                      }
                    );
                  }}>
                  {state.selectedStatus === 'Chọn tình trạng' ? (
                    <Text>{state.selectedStatus}</Text>
                  ) : (
                    <Text>{list[state.selectedStatus]}</Text>
                  )}
                </TouchableOpacity>
              </Row>
              <Row style={styles.row}>
                <TouchableOpacity
                  onPress={() => {
                    ActionSheet.show(
                      {
                        options: ['Miễn phí', 'Có phí'],
                        cancelButtonIndex: CANCEL_INDEX,
                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                        title: 'Hình thức thanh toán',
                      },
                      (buttonIndex) => {
                        if (buttonIndex < 2) {
                          setState({
                            ...state,
                            selectedPayment: listPayment[buttonIndex],
                          });
                        } else {
                          setState({
                            ...state,
                            selectedPayment: listPayment[0],
                          });
                        }
                      }
                    );
                  }}>
                  {state.selectedPayment === 'Loại thanh toán' ? (
                    <Text>{state.selectedPayment}</Text>
                  ) : (
                    <Text>{list[state.selectedPayment]}</Text>
                  )}
                </TouchableOpacity>
              </Row>
            </Col> */}
              </Grid>
              <Button full success rounded disabled={disable} onPress={onSave}>
                <Text>Lưu</Text>
              </Button>
            </View>
          </Content>
        </View>
      </Container>
    </Modal>
    // </View>
  );
});
const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    // marginTop: 22,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.35,
    justifyContent: 'center',
    padding: 16
  },
  rightBlock: {
    width: Dimensions.get('window').width * 0.45,
    justifyContent: 'center',
    margin: 16
  },
  row: {
    borderBottomWidth: 0.5,
  },
  mainModalView: {
    // height: Dimensions.get('window').height * 0.8,
    // width: Dimensions.get('window').height * 0.6,
    margin: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 20,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  titleTxt: {
    fontWeight: 'bold',
    color: 'grey',
    marginLeft: 10,
  },
  closeBtn: {
    position: 'absolute',
    top: 0,
    right: 0,
    zIndex: 5,
  },
});
export default ModalAddThietBi;
