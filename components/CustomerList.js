import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import CustomerModel from '../models/CustomerModel';

type Props = {
  item: CustomerModel,
  index: number,
  onPressItem: (customer: CustomerModel) => void,
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          padding: 8,
          backgroundColor: backgroundColor,
          marginBottom: 10,
        }}>
        <Text style={{ fontWeight: 'bold' }}>{props.item.TB_HOTEN}</Text>
        {props.item.TB_DIENTHOAI ? (
          <Text>{props.item.TB_DIENTHOAI}</Text>
        ) : null}
        <Text>{props.item.DC_LAPDAT}</Text>
      </Card>
    </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<CustomerModel>,
  onPressItem: (customer: CustomerModel) => void,
};
export const CustomerList = (props: CustomerListProps) => {
  return (
    <FlatList
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        return (
          <CustomerListItem
            item={item}
            index={index}
            onPressItem={props.onPressItem}
          />
        );
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};