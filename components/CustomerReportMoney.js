//@flow
import React from 'react';
import { Text, View, Form, Card } from 'native-base';
import { TouchableOpacity, FlatList } from 'react-native';
import CustomerReportMoneyModel from '../models/CustomerReportMoneyModel';

type Props = {
  item: CustomerReportMoneyModel,
  index: number,
  onPressItem: (customer: CustomerReportMoneyModel) => void,
};

const CustomerListItem = (props: Props) => {
  const backgroundColor = props.index % 2 == 0 ? '#d0d8e8' : '#fff';
  return (
    <TouchableOpacity onPress={() => props.onPressItem(props.item)}>
      <Card
        style={{
          padding: 8,
          backgroundColor: backgroundColor,
        }}>
        <Text style={{ fontWeight: 'bold' }}>
          Số phiếu thu: {props.item.SO_PHIEUTHU}
        </Text>
        <Text>Tên KH: {props.item.TEN_KHACHHANG}</Text>
        <Text>{props.item.DIACHI_LAPDAT}</Text>
        <Text>
          Tổng tiền:{' '}
          {props.item.TONG_TIEN.toString().replace(
            /\B(?=(\d{3})+(?!\d))/g,
            ','
          )}
        </Text>
      </Card>
    </TouchableOpacity>
  );
};

type CustomerListProps = {
  listCustomer: Array<CustomerReportMoneyModel>,
  onPressItem: (customer: CustomerReportMoneyModel) => void,
};
export const CustomerReportMoney = (props: CustomerListProps) => {
  return (
    <FlatList
      data={props.listCustomer}
      renderItem={({ item, index }) => {
        return (
          <CustomerListItem
            item={item}
            index={index}
            onPressItem={props.onPressItem}
          />
        );
      }}
      keyExtractor={(item, index) => 'key' + index}></FlatList>
  );
};
