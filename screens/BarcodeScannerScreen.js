import React from 'react';
import { Container, Content, Thumbnail, Text } from 'native-base';
import { StyleSheet, Dimensions, Vibration, Alert, View } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';

const { width } = Dimensions.get('window');
const qrSize = width * 0.7;

export default function BarcodeScannerScreen({ route, navigation }) {
  const intervalId = React.useRef(0);
  const { user, setUser } = React.useContext(UserContext);
  const [hasPermission, setHasPermission] = React.useState(null);
  const [scanned, setScanned] = React.useState(false);
  const [status, setStatus] = React.useState(false);
  const [state, setState] = React.useState({
    barcode: '',
    cameraType: 'back',
    text: 'Scan Barcode',
    torchMode: 'off',
    type: '',
  });
  const KHieu = route.params.KHieu;

  React.useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      setHasPermission(status === 'granted');
    })();
    intervalId.current = setInterval(() => {
      setScanned(false);
      setStatus(false);
    }, 5000);
    // Specify how to clean up after this effect:
    return function cleanup() {
      clearInterval(intervalId.current);
    };
  }, []);
  React.useEffect(() => {
    if (status === true) {
      const payload = {
        KyHieuPhieuThu: KHieu,
        SoPhieuThu: state.text,
      };
      callAPI(
        ApiMethods.GET_THONGTIN_CUOC,
        user.Token,
        payload,
        processSubmit_TK,
        errorSubmit_TK
      );
    }
  }, [status]);
  const barcodeReceived = (e) => {
    // console.log(e.data.length);
    if(e.data.length > 7){
      e.data = e.data.slice(3);
    }
    // console.log(e.data);
    setScanned(true);
    if (e.data !== state.barcode || e.type !== state.type) Vibration.vibrate();
    setState({
      ...state,
      barcode: e.data,
      //text: `${e.data} (${e.type})`,
      text: `${e.data}`,
      type: e.type,
    });
    setStatus(true);
  };
  const processSubmit_TK = (data) => {
    // console.log(data);
    navigation.replace('CollectMoneyScreen', { data: data[0] });
  };
  const errorSubmit_TK = (e) => {
    navigation.replace('BarcodeScreen');
    Alert.alert('Thông báo', e.message);
  };

  if (hasPermission === null) {
    return <View style={{ justifyContent: 'center', alignItems: 'center' }}><Text>Requesting for camera permission</Text></View>;
  }
  if (hasPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <BarCodeScanner
      onBarCodeScanned={scanned ? undefined : barcodeReceived}
      style={[StyleSheet.absoluteFillObject, styles.container]}>
      <Thumbnail
        large
        square
        style={styles.qr}
        source={require('../assets/img/qr_scanner.png')}
      />
      <Text onPress={() => navigation.pop()} style={styles.cancel}>
        Cancel
      </Text>
    </BarCodeScanner>
  );
}
const opacity = 'rgba(0, 0, 0, .6)';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  qr: {
    width: qrSize,
    height: qrSize,
  },
  cancel: {
    fontSize: width * 0.05,
    textAlign: 'center',
    width: '70%',
    color: 'white',
    position: 'absolute',
    bottom: '15%',
  },
});
