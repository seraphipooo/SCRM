import React, { Component } from 'react';
import {
  Container,
  Content,
  Header,
  Toast,
  Input,
  Form,
  Item,
  Picker,
  Icon,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
  TouchableOpacity,
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';
import { Grid, Col, Row } from 'react-native-easy-grid';
import SearchIcon from '../assets/img/search_icon.png';
import DeviceModel from '../models/DeviceModel';
import { DeviceList } from '../components/DeviceList';

const SearchDeviceScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    timkiemMS: '',
    maSo: '',
    listMaSo: null,
    listData: null,
  });
  React.useEffect(() => {
    setState({ ...state, timkiemMS: '1' });
  }, []);

  const onSubmit = () => {
    if (state.maSo.length < 4) {
      Alert.alert(
        'Thông báo',
        'Vui lòng bạn nhập mã số từ 4 ký tự để tìm kiếm'
      );
    } else {
      const payload = {
        Type: state.timkiemMS,
        Entry: state.maSo,
      };
      callAPI(
        ApiMethods.TIMTHIETBI_CHITIET,
        user.Token,
        payload,
        processSubmit,
        null
      );
    }
  };
  const processSubmit = (data) => {
    if (data.length == 0) {
      Toast.show({
        text: 'Dữ liệu rỗng',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setState({
        ...state,
        listData: data,
      });
    }
  };

  const onValueChange_MaSo = (value: string) => {
    setState({ ...state, timkiemMS: value });
  };
  const onPressItem = (customer: DeviceModel) => {
    if (customer.TB_MATHUEBAO != null) {
      navigation.push('CustomerInformationScreen', { customer });
    } else {
      Alert.alert(
        'Thông báo',
        'Thiết bị không thuộc chi nhánh của bạn hoặc chưa lắp cho khách hàng.'
      );
    }
  };
  return (
    <Container>
      <Header searchBar rounded style={{ padding: 8 }}>
        <Grid>
          <Row>
            <Col style={{ width: Dimensions.get('window').width * 0.3 }}>
              <Item Picker style={{ marginLeft: 0 }}>
                <Picker
                  // style={{ borderWidth: 0.5 }}
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  selectedValue={state.timkiemMS}
                  onValueChange={onValueChange_MaSo}>
                  <Picker.Item label="MS1" value="1" />
                  <Picker.Item label="MS2" value="2" />
                </Picker>
              </Item>
            </Col>
            <Col>
              <Item regular>
                <Input
                  placeholder="Nhập mã số"
                  value={state.maSo}
                  onChangeText={(text) => {
                    setState({ ...state, maSo: text });
                  }}
                  returnKeyType="done"
                  onSubmitEditing={onSubmit}
                />
                <TouchableOpacity style={styles.searchBtn} onPress={onSubmit}>
                  <Thumbnail small source={SearchIcon} />
                </TouchableOpacity>
              </Item>
            </Col>
          </Row>
        </Grid>
      </Header>
      <Content padder>
        {state.listData ? (
          <DeviceList listCustomer={state.listData} onPressItem={onPressItem} />
        ) : (
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontStyle: 'italic', fontWeight: 'bold' }}>
              Nhập mã số 1 hoặc 2 để tìm kiếm thông tin thiết bị.
            </Text>
          </View>
        )}
      </Content>
    </Container>
  );
};
export default SearchDeviceScreen;
const styles = StyleSheet.create({});
