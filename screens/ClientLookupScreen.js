import React, { Component } from 'react';
import {
  Container,
  Content,
  Header,
  View,
  Input,
  Text,
  Button,
  Icon,
  Item,
  Textarea,
  Thumbnail,
} from 'native-base';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { StyleSheet, TouchableOpacity, Platform } from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';
import CustomerSearchPayload from '../models/CustomerSearchPayload';
import SearchIcon from '../assets/img/search_icon.png';
import { CustomerList } from '../components/CustomerList';

const ClientLookupScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    ndxuly: '',
  });
  const onSubmit = () => {
    const payload: CustomerSearchPayload = {
      CN_ID: user.CN_ID,
      query: state.ndxuly,
    };
    callAPI(
      ApiMethods.CUSTOMER_SEARCH,
      user.Token,
      payload,
      processSubmit,
      null
    );
  };

  const processSubmit = (data) => {
    setState({ ...state, data: data });
    console.log(data);
  };
  const onPressItem = (customer: CustomerModel) => {
    navigation.push('CustomerInformationScreen', { customer });
  };
  return (
    <Container>
      <Header searchBar rounded>
        <Item>
          <Input
            placeholder="Mã thuê bao, họ tên, điện thoại, địa chỉ."
            onChangeText={(text) => setState({ ...state, ndxuly: text })}
            returnKeyType='done'
            onSubmitEditing={onSubmit}
          />
          <TouchableOpacity style={styles.searchBtn} onPress={onSubmit}>
            <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-search`} />
          </TouchableOpacity>
        </Item>
      </Header>
      <Content padder>
        {state.data ? (
          <CustomerList listCustomer={state.data} onPressItem={onPressItem} />
        ) : (
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{fontStyle: 'italic', fontWeight: 'bold'}}>Nhập thông tin để tìm kiếm thông tin khách hàng.</Text>
          </View>
        )}
      </Content>
    </Container>
  );
};
export default ClientLookupScreen;
const styles = StyleSheet.create({
  searchBtn: {
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
