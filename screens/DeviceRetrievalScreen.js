import React, { Component } from 'react';
import {
  Container,
  Content,
  Toast,
  Input,
  Form,
  Item,
  Picker,
  Icon,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  Card,
  ListItem,
  CheckBox,
  Body
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { DeviceRetrievalModel } from '../models/DeviceRetrievalModel';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';

const DeviceRetrievalScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    lyDoThuHoi: '',
    loaiThuHoi: '',
    ghiChu: '',
  });
  const [checked, setChecked] = React.useState(false);

  const toggleCheckbox = () => {
    setChecked((checked) => {
      return !checked;
    });
  };
  const { TBTB_ID, CN_ID, callback, type, data } = route.params;
  React.useEffect(() => {
    setState({
      ...state,
      lyDoThuHoi: global.listLich[0].LyDoThuHoi[0].Id,
      loaiThuHoi: 'RECOVER',
    });
  }, []);
  const onSubmit = () => {
    const payload: DeviceRetrievalModel = {
      TBTB_ID: TBTB_ID,
      LyDoThuHoi: state.lyDoThuHoi,
      TT_ID_ThuHoi: state.loaiThuHoi,
      CN_ID: CN_ID,
      GHI_CHU: state.ghiChu,
    };
    // console.log(payload)
    // navigation.navigate('SearchDeviceScreen');
    callAPI(ApiMethods.THUHOI, user.Token, payload, processSubmit, null);
  };
  const processSubmit = (data) => {
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'center',
      textStyle: { textAlign: 'center' },
    });
    callback();
    navigation.pop();
  };
  const onValueChange_LoaiThuHoi = (value: string) => {
    setState({ ...state, loaiThuHoi: value });
  };

  const onValueChange_LyDoThuHoi = (value: string) => {
    setState({ ...state, lyDoThuHoi: value });
  };

  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.title}>Loại thu hồi:</Text>
              </Col>
              <Col>
                <Form>
                  <Item Picker>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.loaiThuHoi}
                      onValueChange={onValueChange_LoaiThuHoi}>
                      <Picker.Item label="Sử dụng lại" value="RECOVER" />
                      <Picker.Item label="Không sử dụng" value="DESTROY" />
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.title}>Lý do thu hồi:</Text>
              </Col>
              <Col>
                <Form>
                  <Item Picker>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.lyDoThuHoi}
                      onValueChange={onValueChange_LyDoThuHoi}>
                      {global.listLich[0].LyDoThuHoi.map((item, index) => {
                        return (
                          <Picker.Item
                            label={item.Text}
                            value={item.Id}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.title}>Ghi chú:</Text>
              </Col>
              <Col>
                <Textarea
                  style={styles.noteBlock}
                  value={state.ghiChu}
                  placeholder="Nội dung ghi chú"
                  rowSpan={2}
                  onChangeText={(text) => {
                    setState({ ...state, ghiChu: text });
                  }}
                />
              </Col>
            </Row>
          </Grid>
        </Card>
        <Button full rounded style={styles.btn} onPress={onSubmit}>
          <Text>Thu hồi thiết bị</Text>
        </Button>
      </Content>
    </Container>
  );
};
export default DeviceRetrievalScreen;
const styles = StyleSheet.create({
  row: {
    marginBottom: 10,
  },
  leftBlock: {
    justifyContent: 'center',
    width: Dimensions.get('window').width * 0.3,
    marginBottom: 10
  },
  btn: {
    marginTop: 10,
    margin: 40,
    marginBottom: 20
  },
  noteBlock: {
    marginLeft: 10,
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.2)'
  },
  title: {
    fontWeight: 'bold'
  }
});
