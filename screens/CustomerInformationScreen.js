import React, { Component } from 'react';
import {
  Container,
  Content,
  Toast,
  Input,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  Accordion,
  Spinner,
  Icon
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
  TouchableOpacity
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import * as Location from 'expo-location';
import UpdateCustomerLocationPayload from '../models/UpdateCustomerLocationPayload';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { CustomerModel } from '../models/CustomerModel';
import { UserContext } from '../context/UserContext';
import UserModel from '../models/UserModel';
import MapView from 'react-native-maps';
import CustomerLocationPayload from '../models/CustomerLocationPayload';
import locationpng from '../assets/img/location.png';
import { CustomerSearchPayload } from '../models/CustomerSearchPayload';
import orangeMarkerImg from '../assets/img/nhakh.png';
import { CustomerTTTBList } from '../components/CustomerTTTBList';
import { CustomerCuocList } from '../components/CustomerCuocList';
import { CustomerThietBiList } from '../components/CustomerThietBiList';
import { ThietBiKDMList } from '../components/ThietBiKDMList'

const CustomerInformationScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const { customer } = route.params;
  const [state, setState] = React.useState({
    loading: true,
    data: null,
    // locationChosen: false,
  });
  const [dataArray, setDataArray] = React.useState([
    { title: 'Thông tin khách hàng', content: '1' },
    { title: 'Thông tin thuê bao', content: '2' },
    { title: 'Thông tin cước', content: '3' },
    { title: 'Thông tin thiết bị', content: '4' },
    { title: 'Thông tin thiết bị không đánh mã', content: '5' },
  ]);
  const [margin, setMargin] = React.useState(1);
  const [enabled, setEnabled] = React.useState(true);
  const [cusRegion, setCusRegion] = React.useState({
    latitude: 10.7725504,
    longitude: 106.6958526,
    latitudeDelta: 0.0027,
    longitudeDelta: 0.0315,
  });
  const [region, setRegion] = React.useState({
    latitude: 10.7725504,
    longitude: 106.6958526,
    latitudeDelta: 0.0027,
    longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.0122
  });
  const myRef = React.useRef(null);
  const [location, setLocation] = React.useState(null);
  const [locationChosen, setLocationChosen] = React.useState(false);
  const [status, setStatus] = React.useState('denied');
  React.useEffect(() => {
    (async () => {
      let { status } = await Location.requestPermissionsAsync();
      setStatus(status);
    })();
  }, []);
  global.refreshCustomerInfo = () => {
    loadData();
    checkRight(user);
  }
  const loadData = () => {
    const payload: CustomerLocationPayload = {
      TB_MATHUEBAO: customer.TB_MATHUEBAO,
      CN_ID: customer.CN_ID,
    };
    callAPI(
      ApiMethods.CUSTOMER_GET_DETAILS,
      user.Token,
      payload,
      processData,
      null
    );
  };
  const processData = (data) => {
    setState({ ...state, loading: false, data: data[0] });
    // console.log(data[0]);
    // if (data[0].LDV_ID === 'INTERNET' || data[0].LDV_ID === 'GPON') {
    //   setDataArray([
    //     { title: 'Thông tin khách hàng', content: '1' },
    //     { title: 'Thông tin thuê bao', content: '2' },
    //     { title: 'Thông tin cước', content: '3' },
    //     { title: 'Thông tin thiết bị', content: '4' },
    //     { title: 'Thông tin thiết bị không đánh mã', content: '5' },
    //   ]);
    // }
    if (data[0].Location == null) {
      Alert.alert(
        'Vui lòng cập nhập tọa độ cho khách hàng!!!',
        'Nhấn vào biểu tượng phía trên góc phải bản đồ để lấy vị trí hiện tại'
      );
      // getCurrentLocation();
    } else {
      const coordsEvent = {
        nativeEvent: {
          coordinate: {
            latitude: data[0].Location.Latitude,
            longitude: data[0].Location.Longitude,
          },
        },
      };
      pickLocationHandler(coordsEvent);
    }
  };
  const pickLocationHandler = (event) => {
    const coords = event.nativeEvent.coordinate;

    setLocationChosen(true);
    myRef.current.animateToRegion({
      latitude: coords.latitude,
      longitude: coords.longitude,
      latitudeDelta: 0.0027,
      longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.0122
    });
    setRegion({
      ...region,
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
  };
  // Hàm kiểm tra quyền cập nhật vị trí khách hàng.
  const checkRight = (user: UserModel) => {
    if (user.QuyenThucThi.includes('ALL_CUSTOMER_LOCATION_UPDATE')) {
      setEnabled(false);
    } else {
      setEnabled(true);
    }
  };
  React.useEffect(() => {
    loadData();
    checkRight(user);
  }, []);

  const onMapReady = () => {
    if (locationChosen) {
      myRef.current.animateToRegion(region);
    }
    else
      getCurrentLocation();
  }

  const getCurrentLocation = async () => {
    if (status === 'denied') {
      Alert.alert('Không truy cập được vị trí', 'Vui lòng cho phép truy cập vị trí hiện tại của thiết bị để hoàn tất tác vụ.')
    }
    else {
      let location = await Location.getCurrentPositionAsync({});
      let _region = {
        ...region,
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      };
      myRef.current.animateToRegion(_region);
      if (margin === 0) {
        setMargin(1);
      }
      else {
        setMargin(0);
      }
    }
  };
  const onPressItem = (TTTB: ListThietBiModel) => {
    if (
      state.data.ListThueBao.find((element) => {
        if (element.TB_MATHUEBAO === TTTB.TB_MATHUEBAO) return element;
      }).TRANGTHAISD_ID !== 'ACTIVE'
    ) {
      Alert.alert('Thông báo', 'Bạn có muốn thu hồi thiết bị không?', [
        {
          text: 'Có',
          onPress: () =>
            navigation.navigate('DeviceRetrievalScreen', {
              TBTB_ID: TTTB.TBTB_ID,
              CN_ID: state.data.CN_ID,
              callback: loadData
            }),
        },
        { text: 'Không', onPress: () => console.log('Cancel Pressed') },
      ]);
    } else {
      Alert.alert('Thông báo', 'Bạn có muốn đổi thiết bị không?', [
        {
          text: 'Có',
          onPress: () =>
            navigation.navigate('ChangeDeviceScreen', {
              TTTB: TTTB,
              CN_ID: state.data.CN_ID,
              PGD_ID: state.data.PGD_ID,
              callback: loadData
            }),
        },
        { text: 'Không', onPress: () => console.log('Cancel Pressed') },
      ]);
    }
  };

  const onPressWifiItem = (TTTB: ListThietBiModel) => {
    Alert.alert('Thông báo', 'Bạn có muốn thu hồi thiết bị không?', [
      {
        text: 'Có',
        onPress: () =>
          navigation.navigate('DeviceRetrievalIndoorScreen', {
            data: TTTB,
            type: 'indoor',
            TBTB_ID: TTTB.TBTB_ID,
            CN_ID: state.data.CN_ID,
            callback: loadData
          }),
      },
      { text: 'Không', onPress: () => console.log('Cancel Pressed') },
    ]);
  }

  const addWifiEquip = () => {
    navigation.push('AddWifiScreen', {
      TBTB_ID: state.data.TB_MATHUEBAO,
      KH_ID: state.data.KH_ID,
      CN_ID: state.data.CN_ID,
      callback: loadData
    });
  }

  const onSubmit = async () => {
    if (status === 'denied') {
      Alert.alert('Không truy cập được vị trí', 'Vui lòng cho phép truy cập vị trí hiện tại của thiết bị để hoàn tất tác vụ.')
    }
    else {
      let location = await Location.getCurrentPositionAsync({});
      setLocationChosen(true);
      setRegion({
        ...region,
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      });
      let _region = {
        ...region,
        latitude: location.coords.latitude,
        longitude: location.coords.longitude,
      };
      myRef.current.animateToRegion(_region);
      const payload: UpdateCustomerLocationPayload = {
        TB_ID: state.data.TB_ID,
        KH_ID: state.data.KH_ID,
        TB_MATHUEBAO: state.data.TB_MATHUEBAO,
        CN_ID: state.data.CN_ID,
        PGD_ID: state.data.PGD_ID,
        Latitude: location.coords.latitude,
        Longitude: location.coords.longitude,
        nd_id: user.nd_id,
        DC_LAPDAT: state.data.DC_LAPDAT,
        GhiChu: state.data.TB_GHICHU,
      };
      callAPI(
        ApiMethods.CUSTOMER_UPDATE_LOCATION,
        user.Token,
        payload,
        processSubmit,
        null
      );
    }
  };
  const processSubmit = (data) => {
    global.refreshHome();
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    // navigation.navigate('Home');
  };
  const _renderHeader = (item, expanded) => {
    return (
      <View style={{
        flexDirection: "row",
        padding: 10,
        justifyContent: "space-between",
        alignItems: "center",
        backgroundColor: 'rgba(63, 81, 181, 1)',
        borderColor: 'white',
        borderTopWidth: 5
      }}>
        <Text style={{ color: '#fff', fontWeight: 'bold' }}>
          {" "}{item.title}
        </Text>
        {expanded
          ? <Icon style={{ fontSize: 15, color: '#fff' }} type="FontAwesome" name="chevron-up" />
          : <Icon style={{ fontSize: 15, color: '#fff' }} type="FontAwesome" name="chevron-down" />}
      </View>
    );
  }
  const _renderContent = (item) => {
    if (item.content === '1') {
      return (
        <Grid>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Mã thuê bao</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.TB_MATHUEBAO}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Họ tên</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.TB_HOTEN}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Điện thoại</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.TB_DIENTHOAI}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Địa chỉ</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.DC_LAPDAT}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Chi nhánh</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.CN_TEN_CHI_NHANH}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Phòng giao dịch</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.MA_PGD}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Email</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.TB_EMAIL}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col style={styles.leftBlock}>
              <Text style={styles.textStyle}>Ghi chú</Text>
            </Col>
            <Col style={styles.rightBlock}>
              <Text>{state.data.TB_GHICHU}</Text>
            </Col>
          </Row>
          <Row style={styles.row}>
            <Col>
              <Button
                full
                warning
                rounded
                style={{ margin: 40, marginTop: 10, marginBottom: 10 }}
                onPress={() => navigation.navigate('CreateSchedule', { MaThueBao: customer.TB_MATHUEBAO, CN_ID: customer.CN_ID }) }>
                <Text>TẠO LỊCH</Text>
              </Button>
            </Col>
          </Row>
        </Grid>
      );
    } else if (item.content === '2') {
      if (state.data.ListThueBao.length > 0) {
        return <CustomerTTTBList listCustomer={state.data.ListThueBao} />;
      }
      else return <View style={{ padding: 8 }}><Text>Không có thông tin.</Text></View>;
    } else if (item.content === '3') {
      if (state.data.ListCuoc.length > 0) {
        return <CustomerCuocList listCustomer={state.data.ListCuoc} />;
      }
      else return <View style={{ padding: 8 }}><Text>Không có thông tin.</Text></View>;
    } else if (item.content === '4') {
      if (state.data.ListThietBi.length > 0) {
        return (
          <CustomerThietBiList
            listTB={state.data.ListThietBi}
            onPressItem={onPressItem}
          />
        );
      }
      else return <View style={{ padding: 8 }}><Text>Không có thông tin.</Text></View>;
    }
    else if (item.content === '5') {
      return (
        <View>
          <Button full
            warning
            rounded
            style={{ margin: 40, marginTop: 10, marginBottom: 0 }}
            onPress={addWifiEquip}>
            <Text>THÊM THIẾT BỊ WIFI</Text>
          </Button>
          <ThietBiKDMList
            listTB={state.data.ListThietBiKDM}
            onPressItem={onPressWifiItem}
          />
        </View>
      );
    }
  };
  return state.loading ? (
    <Spinner />
  ) : (
    <Container>
      <Content padder>
        <Accordion
          dataArray={dataArray}
          animation={true}
          expanded={0}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
        />
        {/* <View style={{ height: Dimensions.get('window').width, marginTop: 10 }}> */}
        <View>
          <MapView
            style={[styles.map, { margin: margin }]}
            initialRegion={region}
            showsMyLocationButton={false}
            showsUserLocation={true}
            onMapReady={onMapReady}
            ref={myRef}
            onPress={getCurrentLocation}>
            {locationChosen ? (<MapView.Marker
              title={'Vị trí khách hàng'}
              coordinate={region}
            // image={orangeMarkerImg}
            />) : null}

          </MapView>
          <Button
            transparent
            style={{ position: 'absolute', top: 5, right: 9, opacity: 0.8 }}
            onPress={getCurrentLocation}>
            <Thumbnail small square source={locationpng} />
          </Button>
        </View>
        <Button
          full
          rounded
          style={styles.btn}
          disabled={enabled}
          onPress={onSubmit}>
          <Text>Cập nhật vị trí khách hàng</Text>
        </Button>
      </Content>
    </Container>
  );
};
export default CustomerInformationScreen;
const styles = StyleSheet.create({
  leftBlock: {
    width: Dimensions.get('window').width * 0.4,
    padding: 8,
  },
  rightBlock: {
    paddingBottom: 8,
    paddingTop: 8,
  },
  row: {
    borderBottomWidth: 0.5,
  },
  textStyle: {
    fontWeight: 'bold',
  },
  map: {
    // ...StyleSheet.absoluteFillObject,
    width: Dimensions.get('window').width * 0.95,
    height: Dimensions.get('window').width,
  },
  btn: {
    marginTop: 10,
    marginLeft: 40,
    marginRight: 40,
  },
});
