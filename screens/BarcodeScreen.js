import React, { Component } from 'react';
import {
  Container,
  Header,
  Content,
  Toast,
  Input,
  Form,
  Item,
  Picker,
  Icon,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  ListItem,
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
  TouchableOpacity,
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import AsyncStorage from '@react-native-community/async-storage';
import { UserContext } from '../context/UserContext';
import { Grid, Col, Row } from 'react-native-easy-grid';
import qrcode from '../assets/img/qrcodescan.png';
import searchIcon from '../assets/img/rounded_search.png';

const BarcodeScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    kyHieu: '',
    soPhieu: '',
    display: 'none',
  });
  React.useEffect(() => {
    _retrieveData();
    setState({ ...state, soPhieu: '' });
  }, []);

  //Lưu dữ liệu ghi nhớ
  const _storeData = async () => {
    try {
      await AsyncStorage.setItem('TASKS', state.kyHieu);
    } catch (error) {
      // Error saving data
    }
  };

  //Lấy dữ liệu ghi nhớ
  const _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('TASKS');
      if (value !== null) {
        setState({ ...state, kyHieu: value, display: 'flex' });
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  const onSubmit_TK = () => {
    _storeData();
    const payload = {
      KyHieuPhieuThu: state.kyHieu,
      SoPhieuThu: state.soPhieu,
    };
    callAPI(
      ApiMethods.GET_THONGTIN_CUOC,
      user.Token,
      payload,
      processSubmit_TK,
      null
    );
  };
  const processSubmit_TK = (data) => {
    navigation.navigate('CollectMoneyScreen', { data: data[0] });
  };
  const onSubmit_Barcode = () => {
    _storeData();
    navigation.navigate('BarcodeScannerScreen', { KHieu: state.kyHieu });
  };

  //Kiểm tra số phiếu rỗng
  const onCheck = () => {
    if (state.soPhieu == '') {
      setState({ ...state, disabled: true });
    }
  };
  return (
    <Container>
      <Content>
        <Grid style={styles.grid}>
          <Row>
            <Col
              style={[
                styles.col,
                { width: Dimensions.get('window').width * 0.8 },
              ]}>
              <Item rounded style={styles.kyHieu}>
                <Input
                  placeholder="Nhập ký hiệu hóa đơn"
                  value={state.kyHieu}
                  onChangeText={(text) => {
                    if (text.length > 0) {
                      setState({ ...state, kyHieu: text, display: 'flex' });
                    } else {
                      setState({ ...state, kyHieu: text, display: 'none' });
                    }
                  }}
                />
              </Item>
            </Col>

          </Row>
          <Row style={{ display: state.display }}>
            <Col style={[styles.col, { width: Dimensions.get('window').width * 0.8 }]}>
              <Item
                rounded
                style={[styles.soPhieu, { display: state.display }]}>
                <Input
                  placeholder="Bắn mã hoặc nhập số phiếu"
                  value={state.soPhieu}
                  onChangeText={(text) => {
                    setState({ ...state, soPhieu: text });
                  }}
                />
                <TouchableOpacity onPress={onSubmit_Barcode}>
                  <Thumbnail small square source={qrcode} />
                </TouchableOpacity>
              </Item>
            </Col>
            <Col style={[styles.col, {paddingTop: 5}]}>
              <TouchableOpacity onPress={onSubmit_TK}>
                <Thumbnail square source={searchIcon} />
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
      </Content>
    </Container>
  );
};
export default BarcodeScreen;
const styles = StyleSheet.create({
  grid: { backgroundColor: 'rgba(63, 81, 181, 0.8)', padding: 8 },
  kyHieu: { backgroundColor: '#fff', paddingLeft: 5 },
  soPhieu: {
    marginTop: 10,
    paddingRight: 8,
    paddingLeft: 5,
    backgroundColor: '#fff',
  },
  col: { justifyContent: 'center', alignItems: 'center' },
});
