//@flow
import React, { Component } from 'react';
import { Container, Content, Text } from 'native-base';
import TimKiemXuatKhoResultListItemModel from '../models/TimKiemXuatKhoResultListItemModel';
import { ListWarehouseExportCancel } from '../components/ListWarehouseExportCancel';

const ListResultsCancelWarehouseExport = ({ route, navigation }) => {
  const data = route.params.data;
  const onPressItem = (customer: TimKiemXuatKhoResultListItemModel) => {
    navigation.push('WarehouseExportCancelScreen', { customer });
  };
  return (
    <Container>
      <Content padder>
        <ListWarehouseExportCancel
          listCustomer={data}
          onPressItem={onPressItem}
        />
      </Content>
    </Container>
  );
};
export default ListResultsCancelWarehouseExport;
