import React, { useState, useEffect } from 'react';
import {
  Container,
  Content,
  View,
  Text,
  Button,
  Item,
  Picker,
  Icon,
  Textarea,
  Body,
  Form,
  Spinner,
  Card,
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import {
  ScrollView,
  StyleSheet,
  Dimensions,
  Alert,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import { UserContext } from '../context/UserContext';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { ScheduleList } from '../components/ScheduleList';
import { ManageBranchCalendarPayload } from '../models/ManageBranchCalendarPayload';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const ManageBranchCalendarScreens = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const intervalId = React.useRef(0);
  const [refreshing, setRefreshing] = React.useState(false);
  const [selectedPGD, setPGD] = useState(global.listLich[0].DanhMucPGD[0].Id);
  const [selectedCN, setCN] = useState(global.CN_ID.toString());
  const [selectedListNV, setSelectedListNV] = useState({
    listNV: global.listLich[0].NhanVienTheoLich.filter((data) => {
      return (
        data.Reference ===
        global.listLich[0].DanhMucPGD.find((element) => {
          if (element.Id === global.listLich[0].DanhMucPGD[0].Id)
            return element;
        }).Id
      );
    }),
    nhanVien: '0',
  });
  const [listSchedule, setListSchedule] = React.useState(null);
  const [state, setState] = React.useState({
    data: null,
    loading: true,
    countQH: 0,
    countDH: 0,
    countLD: 0,
    countBT: 0,
    countTN: 0,
    selectedItems: [],
    countStatus: true
  });

  React.useEffect(() => {
    getListSchedule();
    intervalId.current = setInterval(() => {
      setPGD(global.listLich[0].DanhMucPGD[0].Id);
      getListSchedule();
    }, 300000);
    // Specify how to clean up after this effect:
    return function cleanup() {
      clearInterval(intervalId.current);
    };
  }, []);
  React.useEffect(() => {
    onSelectionNV();
    setState({ ...state, countStatus: true });
  }, [selectedListNV]);
  React.useEffect(() => {
    getListSchedule();
    setState({ ...state, countStatus: true });
  }, [selectedPGD]);
  React.useEffect(() => {
    if (listSchedule != null && state.countStatus === true) {
      count();
    }
  }, [listSchedule]);
  const setListNV = (value) => {
    setSelectedListNV({
      listNV: global.listLich[0].NhanVienTheoLich.filter((data) => {
        return (
          data.Reference ===
          global.listLich[0].DanhMucPGD.find((element) => {
            if (element.Id === value) return element;
          }).Id
        );
      }),
      nhanVien: '0',
    });
  };

  const onSelectionChange_CN = (value: string) => {
    setCN(value);
  }
  const onSelectionChange = (value: string) => {
    setPGD(value);
    setListNV(value);
  };
  const onSelectionChange_NV = (value: string) => {
    setSelectedListNV({ ...selectedListNV, nhanVien: value });
  };
  const onSelectionNV = () => {
    if (selectedListNV.nhanVien === '0') {
      setListSchedule(global.listCNSchedule);
    } else {
      setListSchedule(
        global.listCNSchedule.filter((data) => {
          return data.Data === selectedListNV.nhanVien;
        })
      );
    }
  };

  const count = () => {
    if (listSchedule.length == 0) {
      Alert.alert('Nhân viên bạn muốn tìm hiện tại không có lịch');
    }
    global.listCNScheduleNV = listSchedule;
    var countBT = 0,
      countLM = 0,
      countQH = 0,
      countDH = 0,
      countTN = 0;
    listSchedule.forEach((element) => {
      if (element.Status == 'TIEPNHAN') {
        countTN++;
      } else {
        if (element.Reference == 'LD') countLM++;
        else countBT++;
        if (element.Value >= 240) {
          countQH++;
        } else if (element.Value >= 120 && element.Value < 240) {
          countDH++;
        }
      }
    });
    setState({
      ...state,
      countQH: countQH,
      countDH: countDH,
      countLD: countLM,
      countBT: countBT,
      countTN: countTN,
    });
  };
  const getListSchedule = () => {
    setState({ ...state, loading: true });
    const payload: ManageBranchCalendarPayload = {
      PGD_ID: selectedPGD,
    };
    callAPI(
      ApiMethods.GET_LIST_PGD_SCHEDULES,
      user.Token,
      payload,
      processResult,
      processError
    );
  };
  const processResult = (data) => {
    var countBT = 0,
      countLM = 0,
      countQH = 0,
      countDH = 0,
      countTN = 0;
    data.forEach((element) => {
      if (element.Status == 'TIEPNHAN') {
        countTN++;
      } else {
        if (element.Reference == 'LD') countLM++;
        else countBT++;
        if (element.Value >= 240) {
          countQH++;
        } else if (element.Value >= 120 && element.Value < 240) {
          countDH++;
        }
      }
    });
    setState({
      ...state,
      loading: false,
      countQH: countQH,
      countDH: countDH,
      countLD: countLM,
      countBT: countBT,
      countTN: countTN,
    });
    setListSchedule(data);
    global.listCNSchedule = data;
  };
  const processError = (error) => {
    setState({ ...state, loading: false });
  };

  const onPressItem = (Id: string) => {
    navigation.navigate('DetailedSchedule', {
      scheduleId: Id,
      note: 'branch_calendar',
    });
  };
  const onDetailTiepNhan = () => {
    if (selectedListNV.nhanVien === '0') {
      setListSchedule(
        global.listCNSchedule.filter((data) => {
          return data.Status === 'TIEPNHAN';
        })
      );
    } else {
      setListSchedule(
        global.listCNScheduleNV.filter((data) => {
          return data.Status === 'TIEPNHAN';
        })
      );
    }
    setState({ ...state, countStatus: false });
  };
  const onDetailDenHan = () => {
    if (selectedListNV.nhanVien === '0') {
      setListSchedule(
        global.listCNSchedule.filter((data) => {
          return (
            data.Value >= 120 && data.Value < 240 && data.Status !== 'TIEPNHAN'
          );
        })
      );
    } else {
      setListSchedule(
        global.listCNScheduleNV.filter((data) => {
          return (
            data.Value >= 120 && data.Value < 240 && data.Status !== 'TIEPNHAN'
          );
        })
      );
    }
    setState({ ...state, countStatus: false });
  };
  const onDetailQuaHan = () => {
    if (selectedListNV.nhanVien === '0') {
      setListSchedule(
        global.listCNSchedule.filter((data) => {
          return data.Value >= 240 && data.Status !== 'TIEPNHAN';
        })
      );
    } else {
      setListSchedule(
        global.listCNScheduleNV.filter((data) => {
          return data.Value >= 240 && data.Status !== 'TIEPNHAN';
        })
      );
    }
    setState({ ...state, countStatus: false });
  };
  const onDetailBaoTri = () => {
    if (selectedListNV.nhanVien === '0') {
      setListSchedule(
        global.listCNSchedule.filter((data) => {
          return data.Reference === 'BT' && data.Status !== 'TIEPNHAN';
        })
      );
    } else {
      setListSchedule(
        global.listCNScheduleNV.filter((data) => {
          return data.Reference === 'BT' && data.Status !== 'TIEPNHAN';
        })
      );
    }
    setState({ ...state, countStatus: false });
  };
  const onDetailLapMoi = () => {
    if (selectedListNV.nhanVien === '0') {
      setListSchedule(
        global.listCNSchedule.filter((data) => {
          return data.Reference === 'LD' && data.Status !== 'TIEPNHAN';
        })
      );
    } else {
      setListSchedule(
        global.listCNScheduleNV.filter((data) => {
          return data.Reference === 'LD' && data.Status !== 'TIEPNHAN';
        })
      );
    }
    setState({ ...state, countStatus: false });
  };
  const refreshControl = () => {
    setListSchedule(global.listCNSchedule);
    setSelectedListNV({ ...selectedListNV, nhanVien: '0' })
  };
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setListSchedule(global.listCNSchedule);
    setSelectedListNV({ ...selectedListNV, nhanVien: '0' });
    setPGD(global.listLich[0].DanhMucPGD[0].Id);
    setCN(global.CN_ID.toString());
    wait(1000).then(() => setRefreshing(false));
  }, []);
  const wait = (timeout) => {
    return new Promise((resolve) => {
      setTimeout(resolve, timeout);
    });
  };
  return (
    <Container>
      <Content padder
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }>
        <Grid>
          {/* <Row>
            <Col>
              <Form
                style={{
                  borderTopLeftRadius: 2,
                  borderTopRightRadius: 2,
                  borderWidth: 1,
                  borderColor: '#7c7c7c',
                }}>
                <Item Picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={
                      <Icon
                        name="arrow-down"
                      />
                    }
                    selectedValue={selectedCN}
                    onValueChange={onSelectionChange_CN}>
                    {user.QuyenTruyCap.map((item, index) => {
                      return (
                        <Picker.Item
                          label={item.Text}
                          value={item.Id}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                </Item>
              </Form>
            </Col>
          </Row> */}
          <Row>
            <Col>
              <Form
                style={{
                  borderTopLeftRadius: 2,
                  borderTopRightRadius: 2,
                  borderWidth: 1,
                  borderColor: '#7c7c7c',
                }}>
                <Item Picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={
                      <Icon
                        name="arrow-down"
                      />
                    }
                    selectedValue={selectedPGD}
                    onValueChange={onSelectionChange}>
                    {global.listLich[0].DanhMucPGD.map((item, index) => {
                      return (
                        <Picker.Item
                          label={item.Text}
                          value={item.Id}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                </Item>
              </Form>
            </Col>
          </Row>
          <Row>
            <Col>
              <Form
                style={{
                  borderBottomLeftRadius: 2,
                  borderBottomRightRadius: 2,
                  marginBottom: 2,
                  borderWidth: 1,
                  borderTopWidth: 0,
                  borderColor: '#7c7c7c',
                }}>
                <Item Picker>
                  <Picker
                    mode="dropdown"
                    iosIcon={
                      <Icon
                        name="arrow-down"
                      />
                    }
                    selectedValue={selectedListNV.nhanVien}
                    onValueChange={onSelectionChange_NV}>
                    <Picker.Item label="Lịch tất cả" value="0" />
                    {selectedListNV.listNV.map((item, index) => {
                      return (
                        <Picker.Item
                          label={item.Text}
                          value={item.Text}
                          key={index}
                        />
                      );
                    })}
                  </Picker>
                </Item>
              </Form>
            </Col>
          </Row>
          <Row>
            <Col>
              <TouchableOpacity onPress={onDetailQuaHan}>
                <Card style={[styles.quaHan, styles.btn]}>
                  <Text style={styles.textBtn_1}>{state.countQH}</Text>
                  <Text style={styles.textBtn_2}>QUÁ HẠN</Text>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity onPress={onDetailDenHan}>
                <Card style={[styles.sapDenHan, styles.btn]}>
                  <Text style={styles.textBtn_1}>{state.countDH}</Text>
                  <Text style={styles.textBtn_2}>ĐẾN HẠN</Text>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity onPress={onDetailLapMoi}>
                <Card style={[styles.moiGiao, styles.btn]}>
                  <Text style={styles.textBtn_1}>{state.countLD}</Text>
                  <Text style={styles.textBtn_2}>LẮP ĐẶT</Text>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity onPress={onDetailBaoTri}>
                <Card style={[styles.henLai, styles.btn]}>
                  <Text style={styles.textBtn_1}>{state.countBT}</Text>
                  <Text style={styles.textBtn_2}>BẢO TRÌ</Text>
                </Card>
              </TouchableOpacity>
            </Col>
            <Col>
              <TouchableOpacity onPress={onDetailTiepNhan}>
                <Card style={[styles.tiepNhan, styles.btn]}>
                  <Text style={styles.textBtn_1}>{state.countTN}</Text>
                  <Text style={styles.textBtn_2}>TN</Text>
                </Card>
              </TouchableOpacity>
            </Col>
          </Row>
        </Grid>
        {state.loading ? (
          <Spinner />
        ) : (
          <ScheduleList listSchedule={listSchedule} onPressItem={onPressItem} />
        )}
      </Content>
    </Container>
  );
};
export default ManageBranchCalendarScreens;
const styles = StyleSheet.create({
  textBtn_1: {
    fontWeight: 'bold',
    color: 'white',
  },
  textBtn_2: {
    color: 'white',
    // alignSelf: 'center',
    fontSize: 12,
    fontWeight: '700'
  },
  btn: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 5,
    paddingBottom: 5,
    marginTop: 5,
    height: '90%',
  },
  quaHan: {
    backgroundColor: '#c00000',
  },
  sapDenHan: {
    backgroundColor: '#ffc000',
  },
  moiGiao: {
    backgroundColor: '#fac090',
  },
  henLai: {
    backgroundColor: '#95b3d7',
  },
  tiepNhan: {
    backgroundColor: '#27cd42',
  },
});
