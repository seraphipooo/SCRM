import React, { Component } from 'react';
import {
  Thumbnail,
  View,
  Button,
  Text,
  Form,
  Picker,
  Item,
  Icon,
  Card,
  Textarea,
  Input,
  Container,
  List,
  Content,
  Toast,
  ListItem,
  CheckBox,
  Body,
  Left
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import * as Location from 'expo-location';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import { StyleSheet, FlatList, Dimensions, Alert, TouchableOpacity, Platform, Modal, ScrollView } from 'react-native';
import MapView from 'react-native-maps';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { ReasonModel } from '../models/ReasonModel';
import { CommonModel } from '../models/CommonModel';
import { FinishSchedulePayload } from '../models/FinishSchedulePayload';
import { ScheduleModel } from '../models/ScheduleModel';
import { CustomerLocationPayload } from '../models/CustomerLocationPayload';
import locationpng from '../assets/img/location.png';
import orangeMarkerImg from '../assets/img/nhakh.png';
import { UserContext } from '../context/UserContext';
import AsyncStorage from '@react-native-community/async-storage';
import OpenList from '../components/OpenList';
import SubCardGroupThietBi from '../components/SubCardGroupThietBi';

const FinishScheduleModal = ({ route, navigation }) => {
  var list_LNN = [];
  var list_NN = [];
  const schedule = route.params.schedule;
  const type = route.params.type || null;
  const thietBiSuDung = route.params.thietBiSuDung;
  const { user, setUser } = React.useContext(UserContext);
  const [status, setStatus] = React.useState('denied');
  const [variable, setVariable] = React.useState({
    list_LNN: [],
    list_NN: [],
  });

  const openListMS1 = React.useRef(null);
  const openListMS2 = React.useRef(null);
  const map = React.useRef(null);
  const [margin, setMargin] = React.useState(0);
  const [state, setState] = React.useState({
    reasons: [],
    material: [],
    device: false,
    ndxuly: '',
    loaiTB1: '',
    loaiTB2: '',
    selectedKey: '',
    selectedKeyPV: '',
    selectedKeyLNN: '',
    selectedKeyMaKQXL: '',
  });
  const [indoorMS, setIndoorMS] = React.useState({
    macWifiM: '',
    macWifiC: '',
    macModemM: '',
    macModemC: '',
    downloadSpeed: 0,
    uploadSpeed: 0,
    wifiStt: 'M',
    modemStt: 'M'
  });

  const [data_1, setData_1] = React.useState({
    listDataMS: null,
  });
  const [data_2, setData_2] = React.useState({
    listDataMS: null,
  });

  const [region, setRegion] = React.useState({
    latitude: 10.757399253082067,
    longitude: 106.76181085807956,
    latitudeDelta: 0.0027,
    longitudeDelta: 0.0315,
  })
  const [cusRegion, setCusRegion] = React.useState({
    latitude: 10.7725504,
    longitude: 106.6958526,
    latitudeDelta: 0.0122,
    longitudeDelta: (Dimensions.get('window').width / Dimensions.get('window').height) * 0.0122,
  });
  const [locationChosen, setLocationChosen] = React.useState(false);
  const [cusLocationChosen, setCusLocationChosen] = React.useState(false);
  const [checked, setChecked] = React.useState(false);
  const [modalVisible, setModalVisible] = React.useState(false);

  // image variable 
  const [images, setImages] = React.useState({
    LocalImage: [],
    multipleUrl: [],
    data: [],
  })

  // cap quyen camera
  const getPermissionAsync = async () => {
    if (Platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== 'granted') {
        alert('Sorry, we need camera roll permissions to make this work!');
      }
    }
  };

  // upload hinh, chup hinh
  const _pickImage = async () => {
    if (images.LocalImage.length < 3) {
      let pickerResult = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        base64: true,
        quality: 0.5,
        allowsEditing: false,
        // aspect: [4, 3],
      });
      let imageUri = pickerResult
        ? `data:image/jpg;base64,${pickerResult.base64}`
        : null;
      imageUri && { uri: imageUri };
      images.multipleUrl.push(imageUri);
      setImages({
        ...images,
        LocalImage: images.LocalImage.concat([pickerResult.uri]),
        data: images.data.concat([pickerResult.base64]),
      });
    }
    else {
      alert('Giới hạn 3 hình.')
    }
  };
  const _takePhoto = async () => {
    if (images.LocalImage.length < 3) {
      const { status: cameraPerm } = await Permissions.askAsync(
        Permissions.CAMERA
      );
      const { status: cameraRollPerm } = await Permissions.askAsync(
        Permissions.CAMERA_ROLL
      );
      // only if user allows permission to camera AND camera roll
      if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
        let pickerResult = await ImagePicker.launchCameraAsync({
          base64: true,
          quality: 0.5,
          allowsEditing: false,
          // aspect: [4, 3],
        });
        if (!pickerResult.cancelled) {
          let imageUri = pickerResult
            ? `data:image/jpg;base64,${pickerResult.base64}`
            : null;
          images.multipleUrl.push(imageUri);
          // console.log(pickerResult.base64);
          if (pickerResult.base64 !== undefined) {
            console.log(imageUri);
            setImages({
              ...images,
              LocalImage: images.LocalImage.concat([pickerResult.uri]),
              data: images.data.concat([pickerResult.base64]),
            });
          }
        }
      }
    }
    else {
      alert('Giới hạn 3 hình.')
    }
  };
  const onRemove = (item) => {
    let arr = images.LocalImage;
    let data_ = images.data;
    let index = arr.indexOf(item, 0);
    arr.splice(index, 1);
    data_.splice(index, 1);
    setImages({ ...images, LocalImage: arr, data: data_ });
  };

  const toggleCheckbox = () => {
    setChecked((checked) => {
      return !checked;
    });
  };
  // Ma ket qua xu ly
  const onValueChangeMaKQXL = (value: string) => {
    setState({ ...state, selectedKeyMaKQXL: value });
  };
  // Phạm vi
  const onValueChangePV = (value: string) => {
    list_LNN = global.listFinish[0].LoaiNguyenNhan.filter((data) => {
      return (
        data.Reference ===
        global.listFinish[0].PhamVi.find((element) => {
          if (element.Id === value) return element;
        }).Id
      );
    });
    list_NN = global.listFinish[0].NguyenNhan.filter((data) => {
      return (
        data.Reference ===
        global.listFinish[0].LoaiNguyenNhan.find((element) => {
          if (element.Id === list_LNN[0].Id) return element;
        }).Id
      );
    });
    setVariable({ ...variable, list_LNN: list_LNN, list_NN: list_NN });
    setState({
      ...state,
      selectedKeyPV: value,
      selectedKeyLNN: list_LNN[0].Id,
      selectedKey: list_NN[0].Id,
    });
  };
  // Loại nguyên nhân
  const onValueChangeLNN = (value: string) => {
    list_NN = global.listFinish[0].NguyenNhan.filter((data) => {
      return (
        data.Reference ===
        global.listFinish[0].LoaiNguyenNhan.find((element) => {
          if (element.Id === value) return element;
        }).Id
      );
    });
    setState({ ...state, selectedKeyLNN: value, selectedKey: list_NN[0].Id });
    setVariable({ ...variable, list_NN: list_NN });
  };
  // Nguyên nhân
  const onValueChange = (value: string) => {
    setState({ ...state, selectedKey: value });
  };
  const pickLocationHandler = (event) => {
    // alert(JSON.stringify(event));
    const coords = event.nativeEvent.coordinate;
    ref.current.animateToRegion({
      ...region,
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
    setLocationChosen(true);
    setRegion({
      ...region,
      latitude: coords.latitude,
      longitude: coords.longitude,
    });

  };

  const getCurrentLocation = async () => {
    let location = await Location.getCurrentPositionAsync({});
    let _region = {
      ...region,
      latitude: location.coords.latitude,
      longitude: location.coords.longitude,
    };
    map.current.animateToRegion(_region);
    // setTimeout(() => setMargin(0), 1000);
    if (margin === 0) {
      setMargin(1);
    }
    else {
      setMargin(0);
    }
  };

  React.useEffect(() => {
    // (async () => {
    //   let { status } = await Location.requestPermissionsAsync();
    //   setStatus(status);
    // })();
    // loadLocation();
    resetState();
    getPermissionAsync();
  }, []);
  const resetState = () => {
    if (state.selectedKeyPV === '' && state.selectedKeyMaKQXL === '') {
      setState({
        ...state,
        selectedKeyPV: global.listFinish[0].PhamVi[0].Id,
        selectedKeyMaKQXL: global.listLich[0].KetQuaXuLy[0].Id,
      });
    }
    // this.onValueChangePV();
    list_LNN = global.listFinish[0].LoaiNguyenNhan.filter((data) => {
      return (
        data.Reference ===
        global.listFinish[0].PhamVi.find((element) => {
          if (element.Id === global.listFinish[0].PhamVi[0].Id) return element;
        }).Id
      );
    });
    // if (list_LNN.length > 0 && state.selectedKeyLNN === '') {
    //   setState({ ...state, selectedKeyLNN: list_LNN[0].Id });
    //   setVariable({ ...variable, list_LNN: list_LNN });
    // }
    // this.onValueChangeLNN();
    list_NN = global.listFinish[0].NguyenNhan.filter((data) => {
      return (
        data.Reference ===
        global.listFinish[0].LoaiNguyenNhan.find((element) => {
          if (element.Id === global.listFinish[0].LoaiNguyenNhan[0].Id)
            return element;
        }).Id
      );
    });
    if (list_LNN.length > 0 && list_NN.length > 0 && state.selectedKey === '' && state.selectedKeyLNN === '') {
      setState({ ...state, selectedKey: list_NN[0].Id, selectedKeyLNN: list_LNN[0].Id, selectedKeyMaKQXL: global.listLich[0].KetQuaXuLy[0].Id, });
      setVariable({ ...variable, list_NN: list_NN, list_LNN: list_LNN });
    }
    // alert(JSON.stringify(state))
  };
  // Load vị trí khách hàng
  const loadLocation = () => {
    if (margin === 0) {
      setMargin(1);
    }
    else {
      setMargin(0);
    }
    const payload: CustomerLocationPayload = {
      CN_ID: schedule.CN_ID,
      MaThueBao: schedule.MaThueBao || schedule.TB_MATHUEBAO,
    };
    callAPI(
      ApiMethods.CUSTOMER_GET_LOCATION,
      user.Token,
      payload,
      processLocation,
      null
    );
  };
  const processLocation = (data: Array<Location>) => {

    if (data.length === 0) {
      Toast.show({
        text: 'Vị trí khách hàng chưa được cập nhập.',
        duration: 2000,
        position: 'bottom',
        textStyle: { textAlign: 'center' },
      });
      getCurrentLocation();
    } else {
      //Hien thi tren ban do vi tri khach hang
      map.current.animateToRegion({
        latitude: data[0].Latitude,
        longitude: data[0].Longitude,
      });
    }
  };
  const pickLocationHandler_KH = (event) => {
    const coords = event.nativeEvent.coordinate;
    map.current.animateToRegion({
      ...cusRegion,
      latitude: coords.latitude,
      longitude: coords.longitude,
    });
    setCusLocationChosen(true);
    setCusLocationChosen({
      ...cusRegion,
      latitude: coords.latitude,
      longitude: coords.longitude,
    })
  };
  const onSubmit = async () => {
    await AsyncStorage.removeItem(`@equip_${schedule.yc_id}`);
    if (
      schedule.ten_loai_yeucau === 'Lắp mới' ||
      schedule.co_nguyennhan_hoantat != 1
    ) {
      const payload: FinishSchedulePayload = {
        yc_id: schedule.yc_id,
        ma_ketqua_xuly: state.selectedKeyMaKQXL,
        ma_nguyennhan: 'KHAC',
        ma_yeucau: schedule.ma_yeucau,
        nhom: schedule.nhom,
        nd_id: user.ND_ID,
        noi_dung: state.ndxuly,
        latitude: region.latitude,
        longitude: region.longitude,
        ThietBiSuDung: thietBiSuDung,
      };
      // console.log(payload);
      callAPI(
        ApiMethods.FINISH_SCHEDULE,
        user.Token,
        payload,
        processSubmit,
        errorSubmit
      );
    } else {
      const payload: FinishSchedulePayload = {
        yc_id: schedule.yc_id,
        ma_ketqua_xuly: state.selectedKeyMaKQXL,
        ma_nguyennhan: state.selectedKey,
        ma_yeucau: schedule.ma_yeucau,
        nhom: schedule.nhom,
        nd_id: user.ND_ID,
        noi_dung: state.ndxuly,
        latitude: region.latitude,
        longitude: region.longitude,
        ThietBiSuDung: thietBiSuDung,
      };
      console.log(payload);
      callAPI(
        ApiMethods.FINISH_SCHEDULE,
        user.Token,
        payload,
        processSubmit,
        errorSubmit
      );
    }
  };
  const processSubmit = (data) => {
    global.refreshHome();
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.navigate('Home');
  };
  const errorSubmit = (error) => {
    Toast.show({
      text: 'Cập nhật thất bại' + error.message,
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
  };

  const onSubmitIndoor = async () => {
    await AsyncStorage.removeItem(`@equip_${schedule.yc_id}`);
    const payload = {
      LichId: schedule.LichId,
      MacSau: indoorMS.macModemM,
      MacTruoc: indoorMS.macModemC,
      MacWifi: indoorMS.macWifiM,
      MacWifiCu: indoorMS.macWifiC,
      UploadSau: indoorMS.uploadSpeed,
      DownloadSau: indoorMS.downloadSpeed,
      MaNguyenNhan: state.selectedKey,
      NoiDungXuLy: state.ndxuly,
      CheckOutLatitude: region.latitude,
      CheckOutLongitude: region.longitude,
      ThietBiSuDung: thietBiSuDung,
      KhDongTruoc: checked,
      Images: images.data
    };
    callAPI(
      ApiMethods.FINISH_SCHEDULE_INDOOR,
      user.Token,
      payload,
      processSubmit,
      errorSubmit
    );
  }

  // TÌM KIẾM MAC WIFI 
  const addItemMacWifi = (MS1: string) => {
    if (indoorMS.wifiStt == 'M') {
      setIndoorMS({ ...indoorMS, macWifiM: MS1 });
    }
    else {
      setIndoorMS({ ...indoorMS, macWifiC: MS1 });
    }
  };

  //TÌM KIẾM MAC MODEM MOI
  const addItemModem = (MS2: string) => {
    if (indoorMS.modemStt == 'M') {
      setIndoorMS({ ...indoorMS, macModemM: MS2 });
    }
    else {
      setIndoorMS({ ...indoorMS, macModemC: MS2 });
    }
    console.log('TEST')
  };

  const updateMacWifiM = () => {
    if (indoorMS.macWifiM.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 1 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: indoorMS.macWifiM,
      };
      callAPI(
        ApiMethods.TIMTHIETBI_KDM,
        user.Token,
        payload,
        processMacWifi,
        null
      );
    }
  };
  const updateMacWifiC = () => {
    if (indoorMS.macWifiC.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 1 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: indoorMS.macWifiC,
      };
      callAPI(
        ApiMethods.TIMTHIETBI_KDM,
        user.Token,
        payload,
        processMacWifi,
        null
      );
    }
  };
  const processMacWifi = (data) => {
    if (data.length == 0) {
      Toast.show({
        text: 'Dữ liệu rỗng',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setData_1({ ...data_1, listDataMS: data });
    }
  };

  const updateMacModemC = () => {
    if (indoorMS.macModemC.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 2 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: indoorMS.macModemC,
      };
      callAPI(
        ApiMethods.TIMTHIETBI,
        user.Token,
        payload,
        processMacModem,
        null
      );
    }
  };

  const updateMacModemM = () => {
    if (indoorMS.macModemM.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 2 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: indoorMS.macModemM,
      };
      callAPI(
        ApiMethods.TIMTHIETBI,
        user.Token,
        payload,
        processMacModem,
        null
      );
    }
  };
  const processMacModem = (data) => {
    if (data.length == 0) {
      Toast.show({
        text: 'Dữ liệu rỗng',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setData_2({ ...data_2, listDataMS: data });
    }
  };

  const checkMacWifi = () => {
    if (indoorMS.wifiStt == 'M') {
      if (data_1.listDataMS.length == 1) {
        setIndoorMS({ ...indoorMS, macWifiM: data_1.listDataMS[0].Id });
      } else if (data_1.listDataMS.length > 1) {
        openListMS1.current.showModal();
      } else {
        Alert.alert('Khong tim thay ma so ban yeu cau');
      }
    }
    else {
      if (data_1.listDataMS.length == 1) {
        setIndoorMS({ ...indoorMS, macWifiC: data_1.listDataMS[0].Id });
      } else if (data_1.listDataMS.length > 1) {
        openListMS1.current.showModal();
      } else {
        Alert.alert('Khong tim thay ma so ban yeu cau');
      }
    }
  };

  const checkMacModem = () => {
    if (indoorMS.modemStt == 'M') {
      if (data_2.listDataMS.length == 1) {
        setIndoorMS({ ...indoorMS, macModemM: data_2.listDataMS[0].Id });
      } else if (data_2.listDataMS.length > 1) {
        openListMS2.current.showModal();
      } else {
        Alert.alert('Khong tim thay ma so 2 ban yeu cau');
      }
    }
    else {
      if (data_2.listDataMS.length == 1) {
        setIndoorMS({ ...indoorMS, macModemC: data_2.listDataMS[0].Id });
      } else if (data_2.listDataMS.length > 1) {
        openListMS2.current.showModal();
      } else {
        Alert.alert('Khong tim thay ma so 2 ban yeu cau');
      }
    }
  };

  React.useEffect(() => {
    if (data_1.listDataMS != null) {
      checkMacWifi();
    }
  }, [data_1]);
  React.useEffect(() => {
    if (data_2.listDataMS != null) {
      checkMacModem();
    }
  }, [data_2]);

  let marker = null;
  let marker_KH = null;
  let location = null;

  const _renderImages = () => {
    let image = [];
    images.LocalImage.map((item, index) => {
      image.push(
        <Col key={index}>
          <ListItem>
            <Thumbnail
              square
              large
              key={index}
              source={{ uri: item }}
              style={{ height: 100, width: 100}}
            />
            <TouchableOpacity style={{ position: 'absolute', top: 10, left: 0 }}
              onPress={() => onRemove(item)}>
              <Icon type="FontAwesome" name="times" />
            </TouchableOpacity>
          </ListItem>
        </Col>
      );
    });
    return image;
  }

  if (cusLocationChosen) {
    marker_KH = (
      <MapView.Marker
        title={'Vị trí khách hàng'}
        coordinate={cusRegion}
        image={orangeMarkerImg}
      />
    );
  }
  return (
    <Container>
      <Content padder>
        {type === 'indoor' ?
          (<View>
            <Card style={{ padding: 8 }}>
              <Grid>
                <Row>
                  <Col style={styles.title}><Text style={styles.titleTxt}>HÌNH ẢNH</Text></Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={{ flexDirection: 'row' }}>
                    <TouchableOpacity onPress={_pickImage}>
                      <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-image`} style={{ fontSize: 50, margin: 5 }} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={_takePhoto}>
                      <Icon active name={`${Platform.OS === "ios" ? "ios" : "md"}-camera`} style={{ fontSize: 50, margin: 5 }} />
                    </TouchableOpacity>
                  </Col>
                </Row>
                <Row>
                  {_renderImages()}
                </Row>
              </Grid>
            </Card>
            <Card style={{ padding: 8 }}>
              <Grid>
                <Row>
                  <Col style={styles.title}><Text style={styles.titleTxt}>THIẾT BỊ VẬT TƯ</Text></Col>
                </Row>
                <Row>
                  <Col>
                    <ListItem onPress={toggleCheckbox}>
                      <CheckBox checked={checked} onPress={toggleCheckbox} />
                      <Body>
                        <Text style={{ fontStyle: 'italic' }}>Khách hàng có đóng trước</Text>
                      </Body>
                    </ListItem>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>MAC Wifi cũ</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item style={{ marginLeft: 0 }}>
                        <Input
                          placeholder="Nhập mã số"
                          value={indoorMS.macWifiC}
                          onChangeText={(text) => {
                            setIndoorMS({ ...indoorMS, macWifiC: text, wifiStt: 'C' });
                          }}
                        />
                        <TouchableOpacity style={styles.searchBtn} onPress={updateMacWifiC}>
                          <Icon
                            name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                        </TouchableOpacity>
                      </Item>
                    </Form>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>MAC Wifi mới</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item style={{ marginLeft: 0 }}>
                        <Input
                          placeholder="Nhập mã số"
                          value={indoorMS.macWifiM}
                          onChangeText={(text) => {
                            setIndoorMS({ ...indoorMS, macWifiM: text, wifiStt: 'M' });
                          }}
                        />
                        <TouchableOpacity style={styles.searchBtn} onPress={updateMacWifiM}>
                          <Icon
                            name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                        </TouchableOpacity>
                      </Item>
                    </Form>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>MAC modem cũ</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item style={{ marginLeft: 0 }}>
                        <Input
                          placeholder="Nhập mã số"
                          value={indoorMS.macModemC}
                          onChangeText={(text) => {
                            setIndoorMS({ ...indoorMS, macModemC: text, modemStt: 'C' });
                          }}
                        />
                        <TouchableOpacity style={styles.searchBtn} onPress={updateMacModemC}>
                          <Icon
                            name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                        </TouchableOpacity>
                      </Item>
                    </Form>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>MAC modem mới</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item style={{ marginLeft: 0 }}>
                        <Input
                          placeholder="Nhập mã số"
                          value={indoorMS.macModemM}
                          onChangeText={(text) => {
                            setIndoorMS({ ...indoorMS, macModemM: text, modemStt: 'M' });
                          }}
                        />
                        <TouchableOpacity style={styles.searchBtn} onPress={updateMacModemM}>
                          <Icon
                            name={Platform.OS === 'ios' ? 'ios-search' : 'md-search'} />
                        </TouchableOpacity>
                      </Item>
                    </Form>
                  </Col>
                </Row>
              </Grid>
            </Card>
            <Card style={{ padding: 8 }}>
              <Grid>
                <Row>
                  <Col style={styles.title}><Text style={styles.titleTxt}>KẾT QUẢ THỰC HIỆN</Text></Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>Tốc độ download</Text>
                  </Col>
                  <Col>
                    <Input
                      style={styles.inputStyle}
                      keyboardType='numeric'
                      placeholder="Nhập tốc độ"
                      onChangeText={(text) => {
                        setIndoorMS({ ...indoorMS, downloadSpeed: text });
                      }}
                    />
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>Tốc độ upload</Text>
                  </Col>
                  <Col>
                    <Input
                      style={styles.inputStyle}
                      keyboardType='numeric'
                      placeholder="Nhập tốc độ"
                      onChangeText={(text) => {
                        setIndoorMS({ ...indoorMS, uploadSpeed: text });
                      }}
                    />
                  </Col>
                </Row>
              </Grid>
            </Card>
          </View>
          ) : null}
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.titleTxt}>Kết quả xử lý</Text>
              </Col>
              <Col>
                <Form>
                  <Item Picker style={styles.dropdown}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.selectedKeyMaKQXL}
                      onValueChange={onValueChangeMaKQXL}>
                      {global.listLich[0].KetQuaXuLy.map((item, index) => {
                        return (
                          <Picker.Item
                            label={item.Text}
                            value={item.Id}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            {schedule.ten_loai_yeucau === 'Bảo trì' ||
              schedule.co_nguyennhan_hoantat === 1 || type === 'indoor' ? (
              <View>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>Phạm vi</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item Picker style={styles.dropdown}>
                        <Picker
                          mode="dropdown"
                          iosIcon={<Icon name="arrow-down" />}
                          selectedValue={state.selectedKeyPV}
                          onValueChange={onValueChangePV}>
                          {global.listFinish[0].PhamVi.map((item, index) => {
                            return (
                              <Picker.Item
                                label={item.Text}
                                value={item.Id}
                                key={index}
                              />
                            );
                          })}
                        </Picker>
                      </Item>
                    </Form>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>Loại nguyên nhân</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item Picker style={styles.dropdown}>
                        <Picker
                          mode="dropdown"
                          iosIcon={<Icon name="arrow-down" />}
                          selectedValue={state.selectedKeyLNN}
                          onValueChange={onValueChangeLNN}>
                          {variable.list_LNN.length > 0 ? (
                            variable.list_LNN.map((item, index) => {
                              return (
                                <Picker.Item
                                  label={item.Text}
                                  value={item.Id}
                                  key={index}
                                />
                              );
                            })
                          ) : (
                            <Picker.Item label={''} value={''} key={0} />
                          )}
                        </Picker>
                      </Item>
                    </Form>
                  </Col>
                </Row>
                <Row style={styles.row}>
                  <Col style={styles.leftBlock}>
                    <Text style={styles.titleTxt}>Nguyên nhân</Text>
                  </Col>
                  <Col>
                    <Form>
                      <Item Picker style={styles.dropdown}>
                        <Picker
                          mode="dropdown"
                          iosIcon={<Icon name="arrow-down" />}
                          selectedValue={state.selectedKey}
                          onValueChange={onValueChange}>
                          {variable.list_NN.length > 0 ? (
                            variable.list_NN.map((item, index) => {
                              return (
                                <Picker.Item
                                  label={item.Text}
                                  value={item.Id}
                                  key={index}
                                />
                              );
                            })
                          ) : (
                            <Picker.Item label={''} value={''} key={0} />
                          )}
                        </Picker>
                      </Item>
                    </Form>
                  </Col>
                </Row>
              </View>
            ) : null}
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.titleTxt}>Nội dung xử lý</Text>
              </Col>
              <Col>
                <Textarea
                  style={styles.inputStyle}
                  rowSpan={2}
                  placeholder="Nhập nội dung xử lí"
                  onChangeText={(text) => {
                    setState({ ...state, ndxuly: text });
                  }}
                />
              </Col>
            </Row>
            <Row style={styles.row}>
              <Text style={styles.titleTxt}>Vị trí hiện tại</Text>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.mapBlock}>
                <MapView
                  onMapReady={loadLocation}
                  // region={region}
                  showsUserLocation={true}
                  showsMyLocationButton={true}
                  // style={styles.map}
                  style={[styles.map, { margin: margin }]}
                  initialRegion={cusRegion}
                  ref={map}>
                  {marker_KH}
                </MapView>
                {/* <Button
                  transparent
                  style={styles.btnMap}
                  onPress={getLocationHandler}>
                  <Thumbnail square small source={locationpng} />
                </Button> */}
              </Col>
              <Col
                style={{ width: Dimensions.get('window').width * 0.1 }}></Col>
            </Row>
          </Grid>
        </Card>
        <Button
          rounded
          onPress={() => { setModalVisible(true) }}
          style={{ padding: 10, margin: 10, alignSelf: 'center' }}>
          <Text>Xác nhận</Text>
        </Button>
      </Content>
      <OpenList
        ref={(el) => (openListMS1.current = el)}
        datalist={data_1.listDataMS}
        addItemMaSo1={addItemMacWifi}
      />
       <OpenList
        ref={(el) => (openListMS2.current = el)}
        datalist={data_2.listDataMS}
        addItemMaSo1={addItemModem}
      />
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(false);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.headerModal}>
              <Text style={styles.modalText}>Xác nhận vật tư sử dụng</Text>
            </View>
            <ScrollView style={styles.contentModal}>
              {thietBiSuDung.map((item, index) => (
                <SubCardGroupThietBi group={item} key={index} />
              ))}
            </ScrollView>
            <View
              style={styles.footerModal}>
              {type === 'indoor' ? (
                <Button
                  // bordered
                  rounded
                  success
                  onPress={onSubmitIndoor}
                  style={{ margin: 10, padding: 10 }}>
                  <Text>Lưu</Text>
                </Button>
              ) : (
                <Button
                  rounded
                  success
                  // bordered
                  onPress={onSubmit}
                  style={{ margin: 10, padding: 10 }}>
                  <Text>Lưu</Text>
                </Button>
              )}
              <Button
                rounded
                // bordered
                danger
                onPress={() => setModalVisible(false)}
                style={{ margin: 10, padding: 10 }}>
                <Text>Hủy</Text>
              </Button>
            </View>
          </View>
        </View>
      </Modal>
    </Container>
  );
};
const styles = StyleSheet.create({
  inputStyle: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.4)',
  },
  map: {
    // ...StyleSheet.absoluteFillObject,
    // margin: 2,
    width: Dimensions.get('window').width * 0.9,
    height: Dimensions.get('window').width * 0.8,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.35,
    justifyContent: 'center',
  },
  row: {
    marginTop: 10,
  },
  mapBlock: {
    marginTop: 10,
    // borderWidth: 0.5,
    height: Dimensions.get('window').width * 0.8,
    width: Dimensions.get('window').width * 0.9,
  },
  titleTxt: {
    fontWeight: 'bold',
  },
  btnMap: {
    position: 'absolute',
    top: 4,
    right: 9,
    opacity: 0.7,
  },
  dropdown: {
    marginLeft: 0,
    paddingLeft: 0,
  },
  title: {
    alignItems: 'center'
    // margin: 10
  },
  headerModal: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderColor: 'rgba(0, 0, 0, 0.2)',
    borderBottomWidth: 0.5,
    width: Dimensions.get('window').width * 0.8,
  },
  modalText: {
    fontWeight: 'bold',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  modalView: {
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 5,
  },
  footerModal: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: 10
  },
  contentModal: {
    maxHeight: 250
  }
});
export default FinishScheduleModal;
