//@flow
import React, { Component } from 'react';
import {
  Thumbnail,
  Container,
  Content,
  View,
  Button,
  Text,
  Form,
  Picker,
  Item,
  Icon,
  Textarea,
  Input,
  Toast,
  DatePicker,
  Right,
  Left,
  Card,
} from 'native-base';
import { StyleSheet, Dimensions } from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import MultiSelect from 'react-native-multiple-select';
import TimKiemXuatKhoModel from '../models/TimKiemXuatKhoModel';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';

const SearchScreenCancelWarehouseExport = ({ route, navigation }) => {
  const multiSelect = React.useRef(null);
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    startDate: new Date(),
    endDate: new Date(),
    xuatChoNV: undefined,
    phanLoaiXK: undefined,
    maSo1: '',
    maSo2: '',
    data: null,
    selectedItems: [],
    listNhanVien: [],
    tatCa: [{ Id: '-1', Text: 'Tất cả' }],
  });
  React.useEffect(() => {
    setState({
      ...state,
      listNhanVien: state.tatCa.concat(global.listXuatKho[0].NhanVien),
      phanLoaiXK: 'DEFAULT',
      startDate: addDays(new Date(), -14)
    });
  }, []);

  const setStartDate = (startDate: Date) => {
    setState({ ...state, startDate: startDate });
  };
  const setEndDate = (endDate: Date) => {
    setState({ ...state, endDate: endDate });
  };

  const addDays = (date: Date, days: number) => {
    date.setDate(date.getDate() + days);
    return date;
  };

  const onValueChange_xuatChoNV = (value: string) => {
    setState({ ...state, xuatChoNV: value });
  };
  const onValueChange_phanLoaiXK = (value: string) => {
    setState({ ...state, phanLoaiXK: value });
  };

  const onSubmit = () => {
    if (state.selectedItems.length == 0) {
      const payload: TimKiemXuatKhoModel = {
        MaSo1: state.maSo1,
        MaSo2: state.maSo2,
        StartDate: state.startDate,
        EndDate: state.endDate,
        NV_NHAN: '-1',
        Type: state.phanLoaiXK,
      };
      callAPI(ApiMethods.TIMXUATKHO, user.Token, payload, processSubmit, null);
    } else {
      const payload: TimKiemXuatKhoModel = {
        MaSo1: state.maSo1,
        MaSo2: state.maSo2,
        StartDate: state.startDate,
        EndDate: state.endDate,
        NV_NHAN: state.selectedItems[0].toString(),
        Type: state.phanLoaiXK,
      };
      callAPI(ApiMethods.TIMXUATKHO, user.Token, payload, processSubmit, null);
    }
  };

  const processSubmit = (data) => {
    navigation.navigate('ListResultsCancelWarehouseExport', { data: data });
  };

  const onSelectedItemsChange = (selectedItems) => {
    setState({ ...state, selectedItems: selectedItems });
  };

  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row>
              <Col style={styles.centerBlock}>
                <Text style={styles.textStyle}>Từ ngày</Text>
              </Col>
              <Col style={styles.centerBlock}>
                <Text style={styles.textStyle}>Đến ngày</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.centerBlock}>
                <DatePicker
                  defaultDate={state.startDate}
                  minimumDate={new Date(2000, 1, 1)}
                  maximumDate={new Date(2999, 12, 31)}
                  locale={'en-GB'}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  placeHolderText={state.startDate.getDate() + '/' + (state.startDate.getMonth() + 1) + '/' + state.startDate.getFullYear()}
                  animationType={'fade'}
                  androidMode={'default'}
                  textStyle={{ color: 'green' }}
                  placeHolderTextStyle={{ color: '#d3d3d3' }}
                  onDateChange={setStartDate}
                />
              </Col>
              <Col style={styles.centerBlock}>
                <DatePicker
                  defaultDate={state.endDate}
                  minimumDate={new Date(2000, 1, 1)}
                  maximumDate={new Date(2999, 12, 31)}
                  locale={'en-GB'}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={'fade'}
                  androidMode={'default'}
                  textStyle={{ color: 'green' }}
                  placeHolderTextStyle={{ color: '#d3d3d3' }}
                  onDateChange={setEndDate}
                />
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Xuất cho NV:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <MultiSelect
                  hideTags
                  single={true}
                  fontSize={15}
                  //items={global.listXuatKho[0].NhanVien }
                  items={state.listNhanVien}
                  uniqueKey="Id"
                  ref={(component) => {
                    multiSelect.current = component;
                  }}
                  onSelectedItemsChange={onSelectedItemsChange}
                  selectedItems={state.selectedItems}
                  selectText="Tất cả"
                  searchInputPlaceholderText="Tìm kiếm nhân viên..."
                  // altFontFamily="ProximaNova-Light"
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  textColor="#000"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="Text"
                  searchInputStyle={{ color: '#000' }}></MultiSelect>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số 1:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Item>
                  <Input
                    placeholder="Nhập mã số"
                    value={state.maSo1}
                    onChangeText={(text) => {
                      setState({ ...state, maSo1: text });
                    }}
                  />
                </Item>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số 2:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Item>
                  <Input
                    placeholder="Nhập mã số"
                    value={state.maSo2}
                    onChangeText={(text) => {
                      setState({ ...state, maSo2: text });
                    }}
                  />
                </Item>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Phân loại XK:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Form>
                  <Item Picker style={{ marginLeft: 0 }}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.phanLoaiXK}
                      onValueChange={onValueChange_phanLoaiXK}>
                      <Picker.Item label="Mặc định" value="DEFAULT" />
                      <Picker.Item label="Cố định" value="FIXLINE" />
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
          </Grid>
          <Button full rounded style={styles.btn} onPress={onSubmit}>
            <Text>Tìm kiếm</Text>
          </Button>
        </Card>
      </Content>
    </Container>
  );
};
export default SearchScreenCancelWarehouseExport;
const styles = StyleSheet.create({
  btn: {
    margin: 40,
    marginTop: 10,
    marginBottom: 20,
  },
  row: {
    marginBottom: 15,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.3,
    justifyContent: 'center',
  },
  rightBlock: {
    paddingLeft: 10,
  },
  textStyle: {
    fontWeight: 'bold'
  },
  centerBlock: { alignItems: 'center' },
});
