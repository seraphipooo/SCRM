import React, { Component } from 'react';
import {
  Container,
  Content,
  Toast,
  Input,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  Card,
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { TimKiemXuatKhoResultListItemModel } from '../models/TimKiemXuatKhoResultListItemModel';
import { Grid, Col, Row } from 'react-native-easy-grid';
import { UserContext } from '../context/UserContext';

const WarehouseExportCancelScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const customer = route.params.customer;
  const onSubmit = () => {
    const payload: TimKiemXuatKhoResultListItemModel = {
      ID_XUATKHO_KTS: customer.ID_XUATKHO_KTS,
    };
    callAPI(ApiMethods.HUYXUATKHO, user.Token, payload, processSubmit, null);
  };
  const processSubmit = (data) => {
    global.refreshHome();
    Toast.show({
      text: 'Cập nhật thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.navigate('Home');
  };
  // errorSubmit = (error) => {
  //     Toast.show({
  //         text: "Cập nhật thất bại"+error.message,
  //         duration: 2000,
  //         position: "bottom",
  //         textStyle: { textAlign: "center" },
  //     });
  // }
  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số 1</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{customer.MaSo1}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số 2</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{customer.MaSo2}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Ngày xuất</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{customer.NGAY_XUAT}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Serial</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{customer.SERIAL}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Trạng thái</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{customer.TRANG_THAI}</Text>
              </Col>
            </Row>
          </Grid>
          <Button full rounded style={styles.btn} onPress={onSubmit}>
            <Text>Hủy xuất kho</Text>
          </Button>
        </Card>
      </Content>
    </Container>
  );
};
export default WarehouseExportCancelScreen;
const styles = StyleSheet.create({
  btn: {
    margin: 40,
    marginTop: 10,
    marginBottom: 20
  },
  row: {
    marginBottom: 15,
    paddingBottom: 15,
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.3)'
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.3,
    justifyContent: 'center',
  },
  rightBlock: {
    paddingLeft: 10,
  },
  textStyle: {
    color: 'rgba(0, 0, 0, 0.5)',
  }
});
