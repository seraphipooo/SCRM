import React, { Component } from 'react';
import {
  Container,
  Header,
  Content,
  Right,
  Icon,
  View,
  Text,
  Input,
  Label,
  Item,
  Button,
  DatePicker,
  Thumbnail,
  Spinner
} from 'native-base';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { UserContext } from '../context/UserContext';
import { SearchSchedulePayload } from '../models/SearchSchedulePayload';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import SearchIcon from '../assets/img/search_icon.png';
import CommonModel from '../models/CommonModel';
import { ScheduleList } from '../components/ScheduleList';

const FinishedIndoorCalendar = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    startDate: null,
    endDate: new Date(),
    data: null,
    loading: false
  });
  React.useEffect(() => {
    setState({ ...state, startDate: addDays(new Date(), -7) });
  }, []);
  const setStartDate = (startDate: Date) => {
    setState({ ...state, startDate: startDate });
  };
  const setEndDate = (endDate: Date) => {
    setState({ ...state, endDate: endDate });
  };

  const addDays = (date: Date, days: number) => {
    date.setDate(date.getDate() + days);
    return date;
  };
  const onSubmit = () => {
    setState({ ...state, loading: true });
    const payload: SearchSchedulePayload = {
      nd_theolich_id: user.ND_ID,
      TuNgay: state.startDate,
      DenNgay: state.endDate,
    };
    callAPI(
      ApiMethods.GET_LIST_FINISHED_SCHEDULES_INDOOR,
      user.Token,
      payload,
      processSubmit,
      null
    );
  };
  const processSubmit = (data: Array<CommonModel>) => {
    // navigation.navigate('SearchResultScreen', { data: data });
    setState({ ...state, data: data, loading: false });
  };
  const onPressItem = (Id: string) => {
    navigation.push('DetailedIndoorSchedule', { scheduleId: Id, note: 'branch_calendar' });
  };
  return (
    <Container>
      <Header searchBar rounded>
        <Item>
          <Icon name="ios-search" />
          <DatePicker
            defaultDate={state.startDate}
            minimumDate={new Date(2000, 1, 1)}
            maximumDate={new Date(2999, 12, 31)}
            locale={'en-GB'}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={'fade'}
            androidMode="default"
            placeHolderText="Từ ngày"
            placeHolderTextStyle={{ color: '#d3d3d3' }}
            onDateChange={setStartDate}
          />
        </Item>
        <Item>
          <Icon name="ios-search" />
          <DatePicker
            defaultDate={state.endDate}
            minimumDate={new Date(2000, 1, 1)}
            maximumDate={new Date(2999, 12, 31)}
            locale={'en-GB'}
            timeZoneOffsetInMinutes={undefined}
            modalTransparent={false}
            animationType={'fade'}
            androidMode={'default'}
            placeHolderText="Đến ngày"
            placeHolderTextStyle={{ color: '#d3d3d3' }}
            onDateChange={setEndDate}
          />
        </Item>
        <TouchableOpacity style={styles.searchBtn} onPress={onSubmit}>
          <Thumbnail small source={SearchIcon} />
        </TouchableOpacity>
      </Header>
      <Content padder>
        {state.loading ? (
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontStyle: 'italic', fontWeight: 'bold'}}>Không có danh sách lịch.</Text>
          </View>
        ) : (
          <ScheduleList listSchedule={state.data} onPressItem={onPressItem}/>
        )}
      </Content>
    </Container>
  );
};

export default FinishedIndoorCalendar;
const styles = StyleSheet.create({
  searchBtn: {
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
