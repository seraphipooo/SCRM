import React, { Component } from 'react';
import {
  Container,
  Content,
  Toast,
  Input,
  Text,
  Label,
  View,
  Button,
  Textarea,
  Thumbnail,
  Accordion,
  Card,
} from 'native-base';
import {
  TextInput,
  StyleSheet,
  FlatList,
  Dimensions,
  Alert,
  Image,
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { Grid, Row, Col } from 'react-native-easy-grid';
import { UserContext } from '../context/UserContext';
import DaThuTien from '../assets/img/dttien.png';

export default function CollectMoneyScreen({ route, navigation }) {
  const { user, setUser } = React.useContext(UserContext);
  const [state, setState] = React.useState({
    timkiemMS: '',
    maSo: '',
    tongTien: '',
  });
  const [disabled, setDisabled] = React.useState(false);
  const data = route.params.data;
  React.useEffect(() => {
    if (data.Status >= 0) {
      setDisabled(true);
    }
    setState({
      ...state,
      tongTien: data.TONG_TIEN.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','),
    });
  }, []);
  const onSubmit = () => {
    // console.log(data);
    if (data.CurrentMonthId - data.THANG_ID > 2) {
      Alert.alert(
        'Thông báo',
        'Phiếu thu đã cũ không hợp lệ. Nhân viên liên hệ với chi nhánh để kiểm tra.'
      );
    } else {
      Alert.alert(
        'Thông báo',
        'Vui lòng nhân viên kiểm tra thông tin chính xác trước khi thực hiên thao tác!',
        [
          { text: 'Có', onPress: () => onThuTien() },
          { text: 'Không', onPress: () => console.log('Cancel Pressed') },
        ]
        // { cancelable: true }
      );
    }
  };

  const onThuTien = () => {
    const payload = {
      BC_ID: data.BC_ID,
    };
    // console.log(payload)
    callAPI(ApiMethods.THU_CUOC, user.Token, payload, processSubmit, null);
  };

  const processSubmit = (data) => {
    navigation.navigate('BarcodeScreen');
  };

  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Ký hiệu:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{data.KY_HIEU_PHIEUTHU}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Số phiếu thu:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{data.SO_PHIEUTHU}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã KH:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{data.MA_KHACHHANG}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Tên KH:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{data.TEN_KHACHHANG}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Tên cơ quan:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{data.TEN_COQUAN}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Địa chỉ:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{data.DIACHI_LAPDAT}</Text>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Tổng tiền:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Text>{state.tongTien}</Text>
              </Col>
            </Row>
          </Grid>
          <Button full rounded style={styles.btn} onPress={onSubmit} disabled={disabled}>
            <Text>Thu Tiền</Text>
          </Button>
          {disabled ? (
            <View style={styles.viewAbsolute}>
              <Image source={DaThuTien} />
            </View>
          ) : null}
        </Card>
      </Content>
    </Container>
  );
}

const styles = StyleSheet.create({
  btn: {
    margin: 40,
    marginTop: 10,
    marginBottom: 20
  },
  textStyle: {
    color: 'rgba(0,0,0,0.5)'
  },
  row: {
    marginBottom: 15,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.3,
    justifyContent: 'center',
  },
  rightBlock: {
    paddingLeft: 10,
  },
  viewAbsolute: {
    flex: 1,
    width: '100%',
    backgroundColor: 'transparent',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
