import React, { useState, useEffect } from 'react';
import {
    Container,
    Content,
    Card,
    Text,
    Item,
    Icon,
    Input,
    Form,
    Label,
    Button,
    Toast
} from 'native-base';
import {
    StyleSheet,
} from 'react-native';
import { callAPI, ApiMethods } from '../utils/ApiUtils';
import { UserContext } from '../context/UserContext';

const ChangePassword = () => {
    const { user, setUser } = React.useContext(UserContext);
    const [oldPwd, setOldPwd] = useState({
        data: '',
        status: true
    });
    const [newPwd, setNewPwd] = useState({
        data: '',
        status: true
    });
    const [reNewPwd, setReNewPwd] = useState({
        data: '',
        status: true
    });

    const onSave = () => {
        if (oldPwd.data === newPwd.data) {
            Toast.show({
                text: 'Yêu cầu mật khẩu mới khác mật khẩu cũ.',
                duration: 5000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else if (newPwd.data !== reNewPwd.data) {
            Toast.show({
                text: 'Nhập lại mật khẩu mới không chính xác.',
                duration: 5000,
                position: 'bottom',
                textStyle: { textAlign: 'center' },
            });
        }
        else {
            let payload = {
                OldPassword: oldPwd.data,
                NewPassword: newPwd.data
            }
            callAPI(
                ApiMethods.CHANGE_PASSWORD,
                user.Token,
                payload,
                processData,
                null
            )
        }
    }

    const processData = () => {
        Toast.show({
            text: 'Cập nhật thành công',
            duration: 2000,
            position: 'bottom',
            textStyle: { textAlign: 'center' },
        });
        setUser(null);
    }

    return (
        <Container>
            <Content padder>
                <Card style={{ paddingBottom: 16, paddingRight: 16, paddingLeft: 8 }}>
                    <Form>
                        <Item floatingLabel>
                            <Label style={styles.label}>Nhập mật khẩu cũ</Label>
                            <Input style={styles.input} secureTextEntry={oldPwd.status} value={oldPwd.data} onChangeText={(text) => setOldPwd({ ...oldPwd, data: text })} />
                            {oldPwd.status == true ? (
                                <Icon active name='eye' onPress={() => { setOldPwd({ ...oldPwd, status: !oldPwd.status }) }} />
                            ) : (
                                <Icon active name='eye-off' onPress={() => { setOldPwd({ ...oldPwd, status: !oldPwd.status }) }} />
                            )}
                        </Item>
                        <Item floatingLabel>
                            <Label style={styles.label}>Nhập mật khẩu mới</Label>
                            <Input style={styles.input} secureTextEntry={newPwd.status} value={newPwd.data} onChangeText={(text) => setNewPwd({ ...newPwd, data: text })} />
                            {newPwd.status == true ? (
                                <Icon active name='eye' onPress={() => { setNewPwd({ ...newPwd, status: !newPwd.status }) }} />
                            ) : (
                                <Icon active name='eye-off' onPress={() => { setNewPwd({ ...newPwd, status: !newPwd.status }) }} />
                            )}
                        </Item>
                        <Item floatingLabel>
                            <Label style={styles.label}>Nhập lại mật khẩu mới</Label>
                            <Input style={styles.input} secureTextEntry={reNewPwd.status} value={reNewPwd.data} onChangeText={(text) => setReNewPwd({ ...reNewPwd, data: text })} />
                            {reNewPwd.status == true ? (
                                <Icon active name='eye' onPress={() => { setReNewPwd({ ...reNewPwd, status: !reNewPwd.status }) }} />
                            ) : (
                                <Icon active name='eye-off' onPress={() => { setReNewPwd({ ...reNewPwd, status: !reNewPwd.status }) }} />
                            )}
                        </Item>
                    </Form>
                    <Button
                        full
                        primary
                        style={{ margin: 40, marginBottom: 10 }}
                        onPress={() => onSave()}>
                        <Text>Cập nhật</Text>
                    </Button>
                </Card>
            </Content>
        </Container>
    );
}
export default ChangePassword;
const styles = StyleSheet.create({
    label: {
        fontStyle: 'italic'
    },
    input: {
        margin: 8,
    }
});