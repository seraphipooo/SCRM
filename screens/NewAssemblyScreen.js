//React import here
import * as React from 'react';
//External imports here
import {
  Container,
  Content,
  Card,
  Text,
  View,
  Button,
  Body,
  Toast,
  Item,
  Input,
} from 'native-base';
import { Alert } from 'react-native';
import CardGroupThietBi from '../components/CardGroupThietBi';
import { GroupThietBi, ThietBiRow } from '../models/GroupThietBi';
import ModalAddThietBi from '../components/ModalAddThietBi';
import AsyncStorage from '@react-native-community/async-storage';

const NewAssemblyScreen = ({ route, navigation }) => {
  const schedule = route.params.schedule;
  const [state, setState] = React.useState({
    contractNumber: null,
    groups: route.params.groups || [{
      MaThueBao: schedule.MaThueBao,
      SO_HOPDONG: '',
      ListThietBi: [],
    }],
  });
  // React.useEffect(() => {
  //   console.log(schedule);
  // }, [])
  const modalAddThietBi = React.useRef(null);
  const showModalAddThietBi = (index: number) => () => {
    modalAddThietBi.current.showModal(index);
  };

  const addGroup = () => {
    // console.log(state.groups);
    if (!state.contractNumber) {
      Toast.show({
        text: 'Vui lòng nhân viên nhập số hợp đồng.',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      setState((previousState) => ({
        contractNumber: '',
        groups: [
          ...previousState.groups,
          {
            MaThueBao: '',
            SO_HOPDONG: state.contractNumber,
            ListThietBi: [],
          },
        ],
      }));
    }
  };
  const removeGroup = (index: number) => () => {
    let groups: Array<GroupThietBi> = [...state.groups];
    groups.splice(index, 1);
    setState({ groups });
  };

  const addItem = (index: number, item: ThietBiRow) => {
    let groups: Array<GroupThietBi> = [...state.groups];
    groups[index].ListThietBi.push(item);
    setState({ groups });
  };
  const removeItem = (groupIdx: number) => (index: number) => () => {
    let groups: Array<GroupThietBi> = [...state.groups];
    groups[groupIdx].ListThietBi.splice(index, 1);
    setState({ groups });
  };
  const onSaveData = () => {
    storeData(state.groups);
    // console.log(state.groups);
    if (state.groups.length === 0) {
      Alert.alert(
        'Thông báo',
        'Hiện tại bạn chưa nhập thiết bị bạn có chắc chắn bỏ qua không.',
        [
          {
            text: 'YES',
            onPress: () =>
              navigation.navigate('FinishScheduleModal', {
                schedule: schedule,
                thietBiSuDung: state.groups,
              }),
          },
          { text: 'NO', onPress: () => console.log('Cancel Pressed') },
        ]
        // { cancelable: true }
      );
    } else {
      navigation.navigate('FinishScheduleModal', {
        schedule: schedule,
        thietBiSuDung: state.groups,
      });
    }
  };
  const storeData = async (data) => {
    try {
      await AsyncStorage.setItem(`@equip_${schedule.yc_id}`, JSON.stringify(data));
    } catch (error) {
      // Error saving data
    }
  };
  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          {schedule.MaThueBao == '' ?
            (<Item>
              <Input
                placeholder="Nhập mã số hợp đồng"
                onChangeText={(contractNumber) =>
                  setState({ ...state, contractNumber })
                }
                value={state.contractNumber}></Input>
              <Button onPress={addGroup}>
                <Text> + </Text>
              </Button>
            </Item>) :
            null}
          {state.groups.map((item, index) => (
            <CardGroupThietBi
              group={item}
              key={index}
              showModal={showModalAddThietBi(index)}
              removeGroup={removeGroup(index)}
              removeItem={removeItem(index)}
            />
          ))}
          <ModalAddThietBi
            ref={(el) => (modalAddThietBi.current = el)}
            addItem={addItem}
          />
          <Button
            full
            rounded
            style={{ margin: 40, marginBottom: 10, marginTop: 20 }}
            onPress={onSaveData}>
            <Text>Lưu</Text>
          </Button>
        </Card>
      </Content>
    </Container>
  );
};
export default NewAssemblyScreen;