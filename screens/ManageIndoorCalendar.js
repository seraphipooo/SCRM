import React, { useState, useEffect } from 'react';
import {
    Container,
    Content,
    View,
    Text,
    Button,
    Item,
    Picker,
    Icon,
    Textarea,
    Body,
    Form,
    Spinner,
    Card,
} from 'native-base';
import { Grid, Col, Row } from 'react-native-easy-grid';
import {
    ScrollView,
    StyleSheet,
    Dimensions,
    Alert,
    TouchableOpacity,
    RefreshControl,
} from 'react-native';
import { UserContext } from '../context/UserContext';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import { ScheduleList } from '../components/ScheduleList';

const deviceWidth = Dimensions.get('window').width;
const deviceHeight = Dimensions.get('window').height;

const ManageIndoorCalendar = ({ route, navigation }) => {
    const { user, setUser } = React.useContext(UserContext);
    const intervalId = React.useRef(0);
    const [refreshing, setRefreshing] = React.useState(false);
    const [selectedPGD, setPGD] = useState(global.listLich[0].DanhMucPGD[0].Id);
    const [selectedCN, setCN] = useState(global.CN_ID.toString());

    const [listSchedule, setListSchedule] = React.useState(null);
    const [state, setState] = React.useState({
        data: null,
        loading: true,
        countQH: 0,
        countDH: 0,
        countLD: 0,
        countBT: 0,
        countTN: 0,
        selectedItems: [],
        countStatus: true
    });

    React.useEffect(() => {
        getListSchedule();
        intervalId.current = setInterval(() => {
            setPGD(global.listLich[0].DanhMucPGD[0].Id);
            getListSchedule();
        }, 300000);
        // Specify how to clean up after this effect:
        return function cleanup() {
            clearInterval(intervalId.current);
        };
    }, []);
    const onSelectionChange = (value: string) => {
        setPGD(value);
    };

    const count = () => {
        if (listSchedule.length == 0) {
            
        }
        global.listCNScheduleNV = listSchedule;
        var countBT = 0,
            countLM = 0,
            countQH = 0,
            countDH = 0,
            countTN = 0;
        listSchedule.forEach((element) => {
            if (element.Status == 'TIEPNHAN') {
                countTN++;
            } else {
                if (element.Reference == 'LD') countLM++;
                else countBT++;
                if (element.Value >= 240) {
                    countQH++;
                } else if (element.Value >= 120 && element.Value < 240) {
                    countDH++;
                }
            }
        });
        setState({
            ...state,
            countQH: countQH,
            countDH: countDH,
            countLD: countLM,
            countBT: countBT,
            countTN: countTN,
        });
    };
    const getListSchedule = () => {
        setState({ ...state, loading: true });
        const payload: ManageBranchCalendarPayload = {
            // PGD_ID: selectedPGD,
        };
        callAPI(
            ApiMethods.GET_LIST_MY_SCHEDULES_INDOOR,
            user.Token,
            payload,
            processResult,
            processError
        );
    };
    const processResult = (data) => {
        var countBT = 0,
            countLM = 0,
            countQH = 0,
            countDH = 0,
            countTN = 0;
        data.forEach((element) => {
            if (element.Status == 'TIEPNHAN') {
                countTN++;
            } else {
                if (element.Reference == 'LD') countLM++;
                else countBT++;
                if (element.Value >= 240) {
                    countQH++;
                } else if (element.Value >= 120 && element.Value < 240) {
                    countDH++;
                }
            }
        });
        setState({
            ...state,
            loading: false,
            countQH: countQH,
            countDH: countDH,
            countLD: countLM,
            countBT: countBT,
            countTN: countTN,
        });
        setListSchedule(data);
        global.listCNSchedule = data;
    };
    const processError = (error) => {
        setState({ ...state, loading: false });
    };

    const onPressItem = (Id: string) => {
        navigation.navigate('DetailedIndoorSchedule', {
            scheduleId: Id,
            note: 'branch_calendar',
        });
    };
    const onDetailXuLy = () => {
        
    };
    const onDetailHoanTat = () => {
        
    };
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setListSchedule(global.listCNSchedule);
        setPGD(global.listLich[0].DanhMucPGD[0].Id);
        wait(1000).then(() => setRefreshing(false));
    }, []);
    const wait = (timeout) => {
        return new Promise((resolve) => {
            setTimeout(resolve, timeout);
        });
    };
    return (
        <Container>
            <Content padder
                refreshControl={
                    <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
                }>
                <Grid style={{marginBottom: 10}}>
                    <Row>
                        <Col>
                            <Form
                                style={{
                                    borderTopLeftRadius: 2,
                                    borderTopRightRadius: 2,
                                    borderWidth: 1,
                                    borderColor: '#7c7c7c',
                                }}>
                                <Item Picker>
                                    <Picker
                                        mode="dropdown"
                                        iosIcon={
                                            <Icon
                                                name="arrow-down"
                                            />
                                        }
                                        selectedValue={selectedPGD}
                                        onValueChange={onSelectionChange}>
                                        {global.listLich[0].DanhMucPGD.map((item, index) => {
                                            return (
                                                <Picker.Item
                                                    label={item.Text}
                                                    value={item.Id}
                                                    key={index}
                                                />
                                            );
                                        })}
                                    </Picker>
                                </Item>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <TouchableOpacity onPress={onDetailXuLy}>
                                <Card style={[styles.xuLy, styles.btn]}>
                                    <Text style={styles.textBtn_1}>{state.countQH}</Text>
                                    <Text style={styles.textBtn_2}>LỊCH ĐANG XỬ LÝ</Text>
                                </Card>
                            </TouchableOpacity>
                        </Col>
                        <Col>
                            <TouchableOpacity onPress={onDetailHoanTat}>
                                <Card style={[styles.hoanTat, styles.btn]}>
                                    <Text style={styles.textBtn_1}>{state.countDH}</Text>
                                    <Text style={styles.textBtn_2}>LỊCH ĐÃ HOÀN TẤT</Text>
                                </Card>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Grid>
                {state.loading ? (
                    <Spinner />
                ) : (
                    <ScheduleList listSchedule={listSchedule} onPressItem={onPressItem} />
                )}
            </Content>
        </Container>
    );
};
export default ManageIndoorCalendar;
const styles = StyleSheet.create({
    textBtn_1: {
        fontWeight: 'bold',
        color: 'white',
    },
    textBtn_2: {
        color: 'white',
        //alignSelf: 'center',
    },
    btn: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 5,
        paddingBottom: 5,
        marginTop: 5,
        height: '90%',
    },
    xuLy: {
        backgroundColor: '#ffc000',
    },
    hoanTat: {
        backgroundColor: '#27cd42',
    },
});
