//@flow
import React, { Component } from 'react';
import {
  Thumbnail,
  Container,
  Content,
  View,
  Button,
  Text,
  Form,
  Picker,
  Item,
  Icon,
  Textarea,
  Input,
  Toast,
  DatePicker,
  Right,
  Left,
  Card,
} from 'native-base';
import {
  StyleSheet,
  FlatList,
  ScrollView,
  Alert,
  TouchableOpacity,
  Dimensions,
} from 'react-native';
import { Grid, Col, Row } from 'react-native-easy-grid';
import MultiSelect from 'react-native-multiple-select';
import { ApiMethods, callAPI } from '../utils/ApiUtils';
import WarehouseManagementPayload from '../models/WarehouseManagementPayload';
import search from '../assets/img/search_icon.png';
import OpenList from '../components/OpenList';
import EnterTheBranchWarehousePayload from '../models/EnterTheBranchWarehousePayload';
import { UserContext } from '../context/UserContext';

const WarehouseManagementScreen = ({ route, navigation }) => {
  const { user, setUser } = React.useContext(UserContext);
  const [listNhomCuoc, setList] = React.useState([]);
  const openList = React.useRef(null);
  const multiSelect = React.useRef(null);
  const [state, setState] = React.useState({
    hinhThuc: undefined,
    boGiaiMa: undefined,
    data: [],
    xuatChoNV: undefined,
    nhomCuoc: undefined,
    chosenDate: new Date(),
    ghiChu: '',
    phieuXuat: '',
    serial: '',
    maSo1: '',
    maSo2: '',
    ldv_id: '',
    selectedItems: [],
  });
  const [data, setData] = React.useState(null);
  const setChosenDate = (chosenDate: Date) => {
    setState({ ...state, chosenDate: chosenDate });
  };
  const onValueChange_hinhThuc = (value: string) => {
    setState({ ...state, hinhThuc: value });
  };
  const onValueChange_boGiaiMa = (value: string) => {
    setState({
      ...state,
      boGiaiMa: value,
      ldv_id: global.listXuatKho[0].BoGiaiMa.find((element) => {
        if (element.Id === value) return element;
      }).Value,
    });
    setList(
      global.listXuatKho[0].NhomCuoc.filter((data) => {
        return (
          data.Reference ===
          global.listXuatKho[0].BoGiaiMa.find((element) => {
            if (element.Id === value) return element;
          }).Id
        );
      })
    );

  };
  React.useEffect(() => {
    if (listNhomCuoc.length > 0) {
      setState({ ...state, nhomCuoc: listNhomCuoc[0].Id });
    }
  }, [listNhomCuoc])
  const onValueChange_xuatChoNV = (value: string) => {
    setState({ ...state, xuatChoNV: value });
  };
  const onValueChange_nhomCuoc = (value: string) => {
    setState({ ...state, nhomCuoc: value });
  };
  const addItemMaSo1 = (MS1: string) => {
    setState({ ...state, maSo1: MS1 });
  };
  const onSubmit = () => {
    if (state.selectedItems.length == 0) {
      Toast.show({
        text: 'Vui lòng chọn nhân viên xuất',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    } else {
      const payload: WarehouseManagementPayload = {
        MaSo1: state.maSo1,
        MaSo2: state.maSo2,
        PHIEU_XUAT: state.phieuXuat,
        NGAY_XUAT: state.chosenDate,
        NV_XUAT: user.ND_ID,
        NV_NHAN: state.selectedItems[0].toString(),
        GHI_CHU: state.ghiChu,
        CHINHANH_ID: user.CN_ID,
        LDV_ID: state.ldv_id,
        GCCT_ID: state.nhomCuoc,
        BOGIAIMA_ID: state.boGiaiMa,
      };
      callAPI(ApiMethods.XUATKHO, user.Token, payload, processSubmit, null);
    }
  };
  const processSubmit = (data) => {
    Toast.show({
      text: 'Xuất kho thành công',
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
    navigation.navigate('WarehouseManagementScreen');
  };
  const errorSubmit = (error) => {
    Toast.show({
      text: 'Cập nhật thất bại' + JSON.stringify(error),
      duration: 2000,
      position: 'bottom',
      textStyle: { textAlign: 'center' },
    });
  };
  // Button Mã số 1
  const onMaSo1 = () => {
    checkSearch();
  };
  const ktra = () => {
    if (data.length == 1) {
      // openList.current.onClose();
      setState({ ...state, maSo1: data[0].Id });
    } else if (data.length > 1) {
      openList.current.showModal();
    } else {
      Toast.show({
        text: 'Khong tim thay ma so 1 ban yeu cau',
        duration: 2000,
        position: 'top',
        textStyle: { textAlign: 'center' },
      });
    }
  };
  // Button Mã số 2
  const onMaSo2 = () => { };
  // tìm kiếm mã số 1
  const checkSearch = (text) => {
    if (state.maSo1.length < 6) {
      Alert.alert('Vui lòng bạn nhập mã số 1 từ 6 ký tự để tìm kiếm');
    } else {
      const payload: EnterTheBranchWarehousePayload = {
        Type: '1',
        Entry: state.maSo1,
      };
      callAPI(ApiMethods.TIMTHIETBI, user.Token, payload, processTKMS1, null);
    }
  };
  const processTKMS1 = (data) => {
    setData(data);
  };
  React.useEffect(() => {
    if (data !== null) {
      ktra();
    }
  }, [data]);
  // tìm kiếm mã số 1=2
  // checkSearch2 = (text) =>{
  //     const payload:EnterTheBranchWarehousePayload = {
  //         Type:'2',
  //         Entry:this.state.maSo2
  //     }
  //     callAPI(ApiMethods.TIMTHIETBI, global.user.Token, payload, this.processTKMS2, null);
  // }
  // processTKMS2 = (data) => {
  //     this.setState({
  //         listData:data
  //     }, ()=>this.ktra2())
  // }

  // tìm kiếm nhanh Xuất cho nhân viên

  const onSelectedItemsChange = (selectedItems) => {
    setState({ ...state, selectedItems: selectedItems });
  };
  return (
    <Container>
      <Content padder>
        <Card style={{ padding: 8 }}>
          <Grid>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số 1:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Form>
                  <Item style={{ marginLeft: 0 }}>
                    <Input
                      placeholder="Nhập mã số"
                      placeholderTextColor={'rgba(0,0,0,0.2)'}
                      value={state.maSo1}
                      onChangeText={(text) => {
                        setState({ ...state, maSo1: text });
                      }}
                    />
                    <TouchableOpacity
                      style={styles.searchBtn}
                      onPress={onMaSo1}>
                      <Thumbnail small source={search} />
                    </TouchableOpacity>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Mã số 2:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Item>
                  <Input
                    placeholder="Nhập mã số"
                    placeholderTextColor={'rgba(0,0,0,0.2)'}
                    onChangeText={(text) => {
                      setState({ ...state, maSo2: text });
                    }}
                  />
                </Item>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Ngày xuất:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <DatePicker
                  defaultDate={state.chosenDate}
                  minimumDate={new Date(2000, 1, 1)}
                  maximumDate={new Date(2999, 12, 31)}
                  locale={'en-GB'}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={'fade'}
                  androidMode={'default'}
                  textStyle={{ color: 'green' }}
                  placeHolderTextStyle={{ color: '#d3d3d3' }}
                  onDateChange={setChosenDate}
                />
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Phiếu xuất:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Item>
                  <Input
                    placeholder="Nhập phiếu xuất"
                    placeholderTextColor={'rgba(0,0,0,0.2)'}
                    onChangeText={(text) => {
                      setState({ ...state, phieuXuat: text });
                    }}
                  />
                </Item>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Bộ giải mã:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Form>
                  <Item Picker style={{ marginLeft: 0 }}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.boGiaiMa}
                      onValueChange={onValueChange_boGiaiMa}>
                      {global.listXuatKho[0].BoGiaiMa.map((item, index) => {
                        return (
                          <Picker.Item
                            label={item.Text}
                            value={item.Id}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Xuất cho NV:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <MultiSelect
                  hideTags
                  single={true}
                  fontSize={15}
                  items={global.listXuatKho[0].NhanVien}
                  uniqueKey="Id"
                  ref={(component) => {
                    multiSelect.current = component;
                  }}
                  onSelectedItemsChange={onSelectedItemsChange}
                  selectedItems={state.selectedItems}
                  selectText="Chọn nhân viên"
                  searchInputPlaceholderText="Tìm kiếm nhân viên..."
                  // altFontFamily="ProximaNova-Light"
                  tagRemoveIconColor="#CCC"
                  tagBorderColor="#CCC"
                  tagTextColor="#CCC"
                  textColor="#000"
                  selectedItemTextColor="#CCC"
                  selectedItemIconColor="#CCC"
                  itemTextColor="#000"
                  displayKey="Text"
                  searchInputStyle={{ color: '#000' }}></MultiSelect>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Chọn nhóm cước:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Form>
                  <Item Picker style={{ marginLeft: 0 }}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      selectedValue={state.nhomCuoc}
                      onValueChange={onValueChange_nhomCuoc}>
                      {listNhomCuoc.map((item, index) => {
                        return (
                          <Picker.Item
                            label={item.Text}
                            value={item.Id}
                            key={index}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                </Form>
              </Col>
            </Row>
            <Row style={styles.row}>
              <Col style={styles.leftBlock}>
                <Text style={styles.textStyle}>Ghi chú:</Text>
              </Col>
              <Col style={styles.rightBlock}>
                <Textarea
                  style={styles.inputStyle}
                  rowSpan={2}
                  placeholder='Nội dung chú thích'
                  placeholderTextColor={'rgba(0,0,0,0.2)'}
                  onChangeText={(text) => {
                    setState({ ...state, ghiChu: text });
                  }}
                />
              </Col>
            </Row>
          </Grid>
          <Button full rounded onPress={onSubmit} style={styles.btn_save}>
            <Text>LƯU XUẤT KHO</Text>
          </Button>
        </Card>
      </Content>
      <OpenList
        ref={(el) => (openList.current = el)}
        datalist={data}
        addItemMaSo1={addItemMaSo1}
      />
    </Container>
  );
};
export default WarehouseManagementScreen;
const styles = StyleSheet.create({
  inputStyle: {
    borderBottomWidth: 0.5
  },
  btn_save: {
    margin: 40,
    marginTop: 10,
    marginBottom: 20,
  },
  row: {
    marginBottom: 15,
  },
  leftBlock: {
    width: Dimensions.get('window').width * 0.3,
    justifyContent: 'center',
  },
  rightBlock: {
    paddingLeft: 10,
  },
  textStyle: {
    // color: 'rgba(0, 0, 0, 1)',
    fontWeight: 'bold',
    fontSize: 14
  },
});
