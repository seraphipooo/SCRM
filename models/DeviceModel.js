export type DeviceModel = {
  MaSo1: string,
  MaSo2: string,
  NGAY_ADD: Date,
  CN_ID: number,
  CN_TEN_CHI_NHANH: string,
  SERIAL: string,
  TRANG_THAI: string,
  BOGIAIMA: string,
  TT_ID: string,
  GHI_CHU: string,
  TB_MATHUEBAO: string,
};
