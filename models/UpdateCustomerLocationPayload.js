//@flow
export type UpdateCustomerLocationPayload = {
  TB_ID: number,
  KH_ID: number,
  TB_MATHUEBAO: string,
  CN_ID: number,
  PGD_ID: number,
  Latitude: number,
  Longitude: number,
  nd_id: number,
  DC_LAPDAT: string,
  GhiChu: string,
};
