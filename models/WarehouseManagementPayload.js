export type WarehouseManagementPayload = {
  MaSo1: string,
  MaSo2: string,
  PHIEU_XUAT: string,
  NGAY_XUAT: Date,
  NV_XUAT: string,
  NV_NHAN: string,
  GHI_CHU: string,
  CHINHANH_ID: string,
  LDV_ID: string,
  GCCT_ID: string,
  BOGIAIMA_ID: string,
  ID_XUATKHO_KTS: number,
};
