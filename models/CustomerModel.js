export type CustomerModel = {
  TB_ID: number,
  KH_ID: number,
  TB_MATHUEBAO: string,
  TB_HOTEN: string,
  TB_DIENTHOAI: string,
  DC_LAPDAT: string,
  PGD_ID: number,
  MA_PGD: string,
  CN_ID: number,
  CN_TEN_CHI_NHANH: string,
  TB_EMAIL: string,
  TB_GHICHU: string,
  Location: {
    Latitude: number,
    Longitude: number,
  },
};
